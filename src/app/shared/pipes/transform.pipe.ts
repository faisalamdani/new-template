import { Pipe, PipeTransform } from '@angular/core';
import { janarasConstant } from '../janars-constants';

@Pipe({
  name: 'transform'
})
export class TransformPipe implements PipeTransform {
  typeArr = janarasConstant;
  transform(value: any): any {
  let item =  this.typeArr.find(element => {
      if(value === element.key)
          return element.value;
    });
    return item.value;
  }


}
