export const janarasConstant = [{
    "key" : "NOT_STARTED",
    "value" : "Not started"
},
{
    "key" : "IN_PROGRESS",
    "value" : "In progress"
},
{
    "key" : "CLOSED",
    "value" : "Closed"
},
{
    "key" : "NO_EDIT_ROLE",
    "value" : "No edit role"
},
{
    "key" : "INDIVIDUAL_EDIT",
    "value" : "INDIVIDUAL EDIT"
},
{
    "key" : "COLLABORATIVE_EDIT",
    "value" : "COLLABORATIVE EDIT"
},
{
    "key" : "SEQUENTIAL_EDIT",
    "value" : "SEQUENTIAL EDIT"
},
{
    "key" : "EDIT_CARD",
    "value" : "EDIT CARD"
},
{
    "key" : "REVIEW_CARD",
    "value" : "REVIEW CARD"
},
{
    "key" : "TRACK_CARD",
    "value" : "TRACK CARD"
},
{
    "key" : "DONE_OR_COMPLETE",
    "value" : "DONE OR COMPLETE"
},
{
    "key" : "DONE",
    "value" : "Done"
},
{
    "key" : "COMPLETE",
    "value" : "Complete"
},
{
    "key" : "ALL",
    "value" : "ALL"
},
{
    "key" : "TEAM",
    "value" : "TEAM"
},
{
    "key" : "INDIVIDUAL",
    "value" : "INDIVIDUAL"
},
{
    "key" : "GOVERNANCE_GROUP",
    "value" : "GOVERNANCE GROUP"
},
{
    "key" : "LEAD",
    "value" : "LEAD"
},
{
    "key" : "FACILITATOR",
    "value" : "FACILITATOR"
},{
    "key" : "PARTICIPANT",
    "value" : "PARTICIPANT"
},
{
    "key" : "",
    "value" : "__"
},
{
    "key" : "SETTLE",
    "value" : "Settle"
},
{
    "key" : "DEFINE",
    "value" : "Define"
},
{
    "key" : "EXPLORE",
    "value" : "Explore"
},
{
    "key" : "OPTIONS",
    "value" : "Options"
},
{
    "key" : "AGREE",
    "value" : "Agree"
} 
];