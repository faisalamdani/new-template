import { CardsDTO } from './card';


export class Stages {
  stageId: string;
  stageName: string;
  stageStandardName: string;
  stageSequenceNumber: number;
  kanbanType: string;
  negotiationId: string;
  negotiationName: string;
  stageStatus: boolean;
  orchestrationCanvasId: string
  orchestrationCanvasName: string
  stageCreatorId: string;
  stageCreatorName: string
  stageCreatorRole: string
  delayDays:number;
  delayTime:number;
  canvasStageId: string;
  stageFlowRule: String ;
  currentCardsInStage: Array<string>;
  hostTenantId: string;
  hostTanantName: string;
  createdDateTime: string;
  cards: Array<CardsDTO>;
}
