import { Stages } from './stages';

export class CanvasDetailsDTO {
    orchestrationCanvasId: string;
    orchestrationCanvasName: string;
    orchestrationDescription: string;
    kanbanType: string;
    orchestratorId: string;
    orchestratorFirstName: string;
    orchestratorLastName: string;
    createdDateTime: string;
    hostTenantId: string;
    stages: Array<Stages>;
}