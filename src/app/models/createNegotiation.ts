export class CreateNegotiationDto {
    userId : string;
    userName : string;
    negotiationName : string;
    negotiationType : string;
    canvasId : string;
    canvasName : string;
    negotiationDescription : string;
    hostTenantId: string;
    hostTenantName : string;


constructor(

    userId : string,
    userName : string,
    negotiationName : string,
    negotiationType : string,
    canvasId : string,
    canvasName : string,
    negotiationDescription : string,
    hostTenantId: string,
    hostTenantName : string,

)
{
    this.negotiationName=negotiationName;
    this.negotiationDescription=negotiationDescription;
    this.hostTenantId=hostTenantId;
    this.userId=userId;
    this.userName=userName;
    this.negotiationType=negotiationType;
    this.canvasId=canvasId;
    this.canvasName=canvasName;
    this.hostTenantName=hostTenantName;

}


}