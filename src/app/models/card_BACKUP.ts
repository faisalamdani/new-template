import { CardRightsDTO } from './cardRights';

export class CardsDTO {
    cardId: string;
    cardName: string;
    stateName: string;
    stateStandardName: string;
    stateSequenceNumber: number;
    cardSequenceNumber: number;
    cardCurrentStatus: string;
    cardStructure: Map<string,object>
    cardCreatorId: string;
    cardCreatorName: string;
    canvasCardId: string;
    userRoleAssigned: string;
    cardEditRule: string;
    cardDistributionRule: string;
    cardRights: Array<CardRightsDTO>;
    cardDelegation: string;
    createdDateTime: string;
    updatedDateTime: boolean;
}