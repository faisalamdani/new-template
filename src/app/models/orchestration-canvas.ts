export class OrchestrationCanvasDto {

  canvasId: string;
  hostTenantId: string;
  orchestrationName: string;
  orchestrationDescription: string;
  kanbanType: string;
  orchestratorId: string;
  orchestratorFirstName: string;
  orchestratorLastName: string;
  createdDateTime: string;

  constructor(
    canvasId: string,
    hostTenantId: string,
    orchestrationName: string,
    orchestrationDescription: string,
    kanbanType: string,
    orchestratorId: string,
    orchestratorFirstName: string,
    orchestratorLastName: string,
    createdDateTime: string,


  ) {
    this.canvasId = canvasId;
    this.hostTenantId = hostTenantId;
    this.orchestrationName = orchestrationName;
    this.orchestrationDescription = orchestrationDescription;
    this.kanbanType = kanbanType;
    this.orchestratorId = orchestratorId;
    this.orchestratorFirstName = orchestratorFirstName;
    this.orchestratorLastName = orchestratorLastName;
    this.createdDateTime = createdDateTime;

  }
}
