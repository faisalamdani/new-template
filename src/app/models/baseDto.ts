export class BaseDTO {
    hostTenantId: string;
    hostTenantName: string;


    constructor(
        hostTenantId: string,
        hostTenantName: string,
    ) {
        this.hostTenantId = hostTenantId;
        this.hostTenantName = hostTenantName;
    }
}