import { CardRightsDTO } from './cardRights';

export class CanvasCardDTO {
    cardId : string;
    cardName: string;
    stateName: string;
    stateStandardName: string;
    stateSequenceNumber: number;
    cardSequenceNumber: number;
    canvasStageId: string;
    stageName: string;
    cardStructure: Map<string,object>;
    stageStandardName: string;
    kanbanType: string;
    orchestrationCanvasId: string;
    orchestrationCanvasName: string;
    orchestratorId: string;
    orchestratorName: string;
    userRoleAssigned: string; 
    cardEditRule: string;
    cardRights: Array<CardRightsDTO>;
    dataManagementRule: DataManagementRule;
    cardDistributionRule: string;
    cardDelegation: boolean;
    hostTenantId: string;
    hostTenantName: string;

}

export class DataManagementRule {
        editable : boolean;
        structureView : string;
        output : string;
}