
export class CardStructure {
    userId: string;
    userFirstName: string;
    userLastName: string;
    userCardStatus: string;
    cardStructure: Map<string,object>;
}

export class TriggerCardDTO {
    cardId: string;
    userId: string;
    cardStructure: Array<CardStructure>
}
