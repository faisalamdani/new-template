import { CardRightsDTO } from './cardRights';

export class CardsDTO {
    cardId: string;
    cardName: string;
    stateName: string;
    stateStandardName: string;
    stateSequenceNumber: number;
    cardSequenceNumber: number;
    cardStatus: string;
    cardStructure: Map<string,object>
    userRoleAssigned: string;
    cardEditRule: string;
    cardDistributionRule: string;
    cardRights: Array<CardRightsDTO>;
    stageSequenceNumber: string;
    createdDateTime: string;
    updatedDateTime: String;
    dueDate: String;
    useDueDate: Boolean;
    comment: string;
}