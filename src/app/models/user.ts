export class User {

    userId: string;
    hostTenantId: string;
    hostTenantName : string;
    organizationId: string;
    firstName: string;
    middleName: string;
    lastName: string;
    email: string
    phone: string;
    role: string;
    cardStatus : string;

    constructor() { }



}
