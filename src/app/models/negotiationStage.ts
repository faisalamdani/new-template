export class NegotiationStage{

    stageInNegotiationId: string;
    negotiationId: string;
    currentStateInNegotiation: string;
    negotiationName: string;
    stageStandardName: string;
    stageCustomName:string;
    stageSequenceNumber:number;
    hostTenantId:string;
    hostTenantName:string;

    constructor(

        stageInNegotiationId: string,
        negotiationId: string,
        currentStateInNegotiation: string,
        negotiationName: string,
        stageStandardName: string,
        stageCustomName:string,
        stageSequenceNumber:number,
        hostTenantId:string,
        hostTenantName:string,

    )
{
    this.stageInNegotiationId = stageInNegotiationId;
    this.negotiationId = negotiationId;
    this.currentStateInNegotiation = currentStateInNegotiation;
    this.negotiationName = negotiationName;
    this.stageStandardName = stageStandardName;
    this.stageCustomName = stageCustomName;
    this.stageSequenceNumber = stageSequenceNumber;
    this.hostTenantId = hostTenantId;
    this.hostTenantName = hostTenantName;

}


}