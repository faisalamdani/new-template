import { CardsDTO } from './card';


export class Stages {
  stageId: string;
  stageName: string;
  stageStandardName: string;
  stageDescription: string;
  stageSequenceNumber: number;
  stageStatus: string;
  stageFlowRule: string;
  totalCards: number;
  cards: Array<CardsDTO>;
}
