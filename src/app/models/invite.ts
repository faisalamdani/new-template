export class inviteDTO{
    teamId: string;
    email:string;
    firstName:string;
    lastName:string;
    tenantId:string;
}