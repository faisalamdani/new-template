export class UserNegotiation{

    userNegotiationId: string;
    negotiationId: string;
    userId:string;
    userName:string;
    negotiationName:string;
    negotiationState:string;
    negotiationStatus:string;
    negotiationRights: Array<string>;
    roleId:string;
    roleName:string;
    roleType:string;

    constructor(

        userNegotiationId: string,
        negotiationId: string,
        userId:string,
        userName:string,
        negotiationName:string,
        negotiationState:string,
        negotiationStatus:string,
        negotiationRights: Array<string>,
        roleId:string,
        roleName:string,
        roleType:string,
    )
    {

        this.userNegotiationId = userNegotiationId;
		this.negotiationId = negotiationId;
		this.userId = userId;
		this.userName = userName;
		this.negotiationName = negotiationName;
		this.negotiationState = negotiationState;
		this.negotiationStatus = negotiationStatus;
		this.negotiationRights = negotiationRights;
		this.roleId = roleId;
		this.roleName = roleName;
		this.roleType = roleType;

    }

}