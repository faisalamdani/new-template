import { Stages } from './stages';
import { TeamDTO } from './team';
import { User } from './user';

export class UserNegotiation {

    negotiationId: string;
    negotiationName: string;
    negotiationDescription: string;
    negotiationStatus: string;
    kanbanType: string;
    creatorId: string;
    creatorFirstName: string;
    creatorLastName: Array<string>;
    creatorRole: string;
    orchestrationCanvasId: string;
    orchestrationCanvasName: string;
    teamName: string;
    users: Array<User>;
    userId: string;
    userFirstName: string;
    userLastName: string;
    userRole: string;
    hostTenantId: string;
    createdDateTime: string;
    updatedDateTime: string;
    stages: Array<Stages>;

}