export class CanvasStageDTO {
  stageId: string;
  stageName: string;
  stageStandardName: string;
  stageDescription: string;
  sequenceNumber: number;
  kanbanType: string;
  orchestrationCanvasId: string;
  orchestrationCanvasName: string;
  stageFlowRule: string;
  totalCardsInStage: boolean;
  // delayDays:number;
  // delayTime:number;
  orchestratorId: string;
  orchestratorName: string;
  hostTenantId: string;
  hostTenantName: string;
}
