export class CardRightsDTO {
	permissionType: string;
	stateRule: string;
    privacyRule: string;
}