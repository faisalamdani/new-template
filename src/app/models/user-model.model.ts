export class UserModel {
    userCardId : String;
    userId : String;
    userFirstName : String;
    userLastName : String;
    userRole : String;
    userRoleAssigned : String;
    cardId : String;
    cardName : String;
    cardStructure : Map<String,Object>;
    negotiationId : String;
    negotiationName : String;
    stageId : String;
    stageName : String;
    orchestrationCanvasId : String;
    orchestrationCanvasName : String;
    teamId : String;
    teamName : String;
    kanbanType : String;
    cardStatus : String;
    cardCreatorId : String;
    cardCreatorName : String;
    cardRights : Array<String>;
    organizationId : String;
    organizationName : String;
    governanceGroupId : String;
    governanceGroupName : String;
    hostTenantId : String;
    hostTenantName : String;
    createdDateTime : String;
    updatedDateTime : String;
    editRule : boolean;
}

