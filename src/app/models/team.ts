import { TeamUsersDTO } from './teamUsers';

export class TeamDTO {
  negotiationId: string;
  canvasId: string;
  teamId: string;
  teamName: string;
  users: Array<TeamUsersDTO>;
  createdDate: string;
  hostTenantId: string;
  creatorId: string;

  // constructor(
  //   teamId: string,
  //   teamName: string,
  //   users: Array<TeamUsersDTO>,
  //   createdDateTime: string
  // ) {

  //   this.teamId = teamId;
  //   this.teamName = teamName;
  //   this.users = users;
  //   this.createdDateTime = createdDateTime;
  // }
}