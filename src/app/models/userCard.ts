export class UserCardDTO {
    userCardId: string;
    userId: string;
    userFirstName: string;
    userLastName: string;
    userRole: string;
    userRoleAssigned: number;
    cardId: string;
    cardName: string;
    cardStructure: Map<string,object>;
    negotiationId: string;
    stageId: string;
    stageName: string;
    stateName: string;
    stateStandardName: String;
    orchestrationCanvasId: string;
    orchestrationCanvasName: String;
    teamId: string;
    teamName: String;
    kanbanType: string;
    cardStatus: String;
    cardEditRule: string;
    hostTenantId: String;
    createdDateTime: string;
    updatedDateTime: String;
}