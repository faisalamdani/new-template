import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
    let idToken = JSON.parse(localStorage.getItem('loginUserData'));

    if (idToken) { 
      return true;
    } else {
      localStorage.clear();
      this.router.navigate(['/']);
      return false;
    }


  }
  
}
