import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthServiceService } from 'src/app/services/auth-service/auth-service.service';
import { User } from 'src/app/models/user';
import {ToastService} from 'ng-uikit-pro-standard'
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  pools: string;
  loading = false;
  submitted = false;
  options = { positionClass: 'md-toast-bottom-right' };

  constructor(
    private router: Router,
    private authservice: AuthServiceService,
    private toast: ToastService,
    private location: Location
  ) {
   }

  ngOnInit() {

    // if(JSON.parse(localStorage.getItem("userdata"))){
    //   this.location.back();
    // }

    const url = window.location.hostname;
    if (url.split(".", 3)[0] == 'nn9bf4hxof' || url.split(".", 3)[0] == '9b2vtkwoxc' || url.split(".", 3)[0] == "localhost") {
      this.pools = "janars";
    } else {
      this.pools = url.split(".", 3)[0];
    }

    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
      pool: new FormControl(this.pools)
    });
  }

  get validation() {
    return this.loginForm.controls;
  }



  loginFormSubmit() {

    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;


    this.authservice.signIn(this.loginForm.value).subscribe((data: any) => {

      if (data.status == "success") {
        if (data.messageData.ChallengeName == "NEW_PASSWORD_REQUIRED") {
          environment.confirm.email = this.loginForm.value.email;
          environment.confirm.password = this.loginForm.value.password;
          this.router.navigate(['/newpassword']);
          this.submitted = false;
          this.loading = false;
        } else {

          this.getUserDetails(this.loginForm.value.email, (data.messageData.pool).split("/", 2)[0]);
          localStorage.setItem('loginUserData', JSON.stringify(data.messageData));

        }
      } else if (data.status == "UserNotConfirmedException") {

        this.router.navigate(['/verification']);
        this.toast.error(data.messageData, '', this.options);
        
        this.submitted = false;
        this.loading = false;

      } else if (data.status == "NotAuthorizedException") {
        if (data.messageData == "Incorrect username or password.") {
         this.toast.error(data.messageData + "You may reset your password.", '', this.options);
          this.submitted = false;
          this.loading = false;
        } else if (data.messageData == "Password attempts exceeded") {
         this.toast.error(data.messageData, '', this.options);
          this.submitted = false;
          this.loading = false;
        } else {
         this.toast.error("Your password is expired please reset your password", '', this.options);
          environment.confirm.email = this.loginForm.value.email;
          environment.confirm.password = "adminReset";
          this.router.navigate(['/forgotpassword']);
        }
      } else {
       this.toast.error(data.messageData, '', this.options);
        this.submitted = false;
        this.loading = false;
      }
    },
      error => {
        this.submitted = false;
        this.loading = false;
      });
  }


  getUserDetails(email: any, tenantId: any) {

    this.authservice.getUserByEmailAndTenant(email.toLowerCase(), tenantId).subscribe((data: User) => {

      if (data.email) {
        this.loading = false;
        this.submitted = false;
        localStorage.setItem('userdata', JSON.stringify(data));

        let user = JSON.parse(localStorage.getItem("userdata"));
     
        if (user.role === 'FACILITATOR') {
          this.router.navigate(['/interplay/interplaylist']);
          this.toast.success('Signed in successfully', '', this.options);
        }
        else if (user.role === 'HOST') {
          this.router.navigate(['/users']);
          this.toast.success('Signed in successfully', '', this.options);
        }
        else if (user.role === 'ORCHESTRATOR') {
          this.router.navigate(['/orchestrations/orchestrationslist']);
          this.toast.success('Signed in successfully', '', this.options);
        }
        else if (user.role === 'PARTICIPANT') {
          this.router.navigate(['/interplay/interplaylist']);
          this.toast.success('Signed in successfully', '', this.options);
        } else {
          localStorage.clear();
          this.router.navigate(['/']);
          this.toast.error('Not allowed', '', this.options);
        }
      } else {
        localStorage.clear();
        this.toast.error('Something went wrong...', '', this.options);
      }
      this.submitted = false;
        this.loading = false;

    },
      error => {
        localStorage.clear();
        this.toast.error('Something went wrong...', '', this.options);
        this.submitted = false;
        this.loading = false;
      });

  }


}
