import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })

export class AuthInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      
        let idToken = JSON.parse(localStorage.getItem('loginUserData'));
        
        if (idToken) {
            req = req.clone({
                setHeaders:
                {
                    authorization: idToken.idToken
                }
            });
        }
        return next.handle(req);
    }
}
