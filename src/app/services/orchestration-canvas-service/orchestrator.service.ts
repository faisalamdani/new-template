import { Injectable } from '@angular/core';
import { ApiService } from '../api-service/api.service';
import { CanvasCardDTO } from 'src/app/models/canvasCard';

@Injectable({
  providedIn: 'root'
})
export class OrchestratorService {

  constructor(private api: ApiService) { }

  getOrchestrationsByTenantId(tenantId: string) {
    return this.api.get('orchestrations/tenant/' + tenantId).pipe();
  }

  getAllOrchestrationsByOrchestratorId(tenantId: string,userId: string) {
    return this.api.get('orchestrations/user/'+userId+'/tenant/'+tenantId).pipe();
  }

  createOrchestrationCanvas(canvas: any) {
    return this.api.post('orchestrations/create', JSON.stringify(canvas)).pipe();
  }

  getAllStagesInOrchestration(canvasId,hostTenantId) {
    return this.api.get('orchestration/' + canvasId + '/tenant/' + hostTenantId).pipe();
  }

  getAllStagesInNegotiation(negotiationId,canvasId) {
    return this.api.get('negotiation/' + negotiationId + '/canvas/' + canvasId +  '/details').pipe();
  }

  createNegotiationHandler(negotiation : any){
    return this.api.post('/negotiation', JSON.stringify(negotiation)).pipe();

  }

  addStageInOrchestration(data: any) {
    return this.api.post('orchestrations/stage/create', data).pipe();
  }

  createCardInOrchestrationCanvas(data: CanvasCardDTO){
    return this.api.post('orchestration/card/create', JSON.stringify(data)).pipe();
  }

  canvasKanbanMoveCard(data){
    return this.api.put('orchestration/stage/card', JSON.stringify(data)).pipe();
    //return this.api.put('orchestration/'+data.canvasId+'/stage/'+data.stageId+'/card/'+data.cardId, JSON.stringify(data)).pipe();
  }
 
  deleteOrchestrationCanvasById(canvas){
    return this.api.delete('orchestrations/'+canvas.canvasId+'/tenant/'+canvas.hostTenantId).pipe();
  }
 
  deleteCanvasStageById(stage,canvasId){
    return this.api.delete('orchestrations/'+canvasId+'/stage/'+stage.stageId).pipe();
  }

  deleteCanvasCard(card,canvasId){
    return this.api.delete('orchestration/'+canvasId+'/card/'+card.cardId).pipe();
  }

  // getCardByCardId(cardId: string, canvasId: string){
  //   return this.api.get('orchestration/'+canvasId+'/card/'+cardId).pipe();
  // }


}
