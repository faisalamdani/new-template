import { Injectable } from '@angular/core';
import { ApiService } from '../api-service/api.service';
//import { AddStageInNegotiationDto } from 'src/app/models/addStageInNegotiation';

@Injectable({
  providedIn: 'root'
})
export class NegotiationService {

  constructor(private api : ApiService) { }

  getUserNegotiationsByUserId(userId:string, tenantId:string){
  //  console.log("Requesting Negotiations by user id : "+userId +"   and tenantId : "+tenantId);
    return new Promise((resolve, reject) => {
      this.api.get('negotiations/user/tenant/'+userId+"/"+tenantId)
      .subscribe(res => {
      //  console.log("getUserNegotiationsByUserId success:" + JSON.stringify(res));
          resolve(res);
        }, (err) => {
        //  console.log("getUserNegotiationsByUserId error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  getNegotiationsByCanvasId(canvasId:string){
    //console.log("Requesting Negotiations by canvas id : "+canvasId);
    return new Promise((resolve, reject) => {
      this.api.get('negotiations/canvas/'+canvasId)
      .subscribe(res => {
      //  console.log("getNegotiationsByCanvasId success:" + JSON.stringify(res));
          resolve(res);
        }, (err) => {
        //  console.log("getNegotiationsByCanvasId error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  getAllNegotiationsByUserId(userId,tenantId) {
    return new Promise((resolve, reject) => {
      this.api.get('negotiations/user/'+userId + '/tenant/' + tenantId)
      .subscribe(res => {
      //  console.log("getNegotiationsByCanvasId success:" + JSON.stringify(res));
          resolve(res);
        }, (err) => {
        //  console.log("getNegotiationsByCanvasId error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  deleteNegotiationCanvasById(negotiationId,userId){
    return this.api.delete('negotiation/'+negotiationId+'/user/'+userId).pipe();
  }

  getStagesofNegotiationById(negotiationId:string){
   // console.log("Requesting Negotiations Details by Negotiation id : "+negotiationId);
    return new Promise((resolve, reject) => {
      this.api.get('negotiation/stages/details/'+negotiationId)
      .subscribe(res => {
       // console.log("getAllStagesofNegotiationById success:" + JSON.stringify(res));
          resolve(res);
        }, (err) => {
         // console.log("getAllStagesofNegotiationById error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

 /* addStageInNegotiation(data : AddStageInNegotiationDto){
   // console.log("Adding stage in negotiation DATA : "+JSON.stringify(data));
    return new Promise((resolve, reject) => {
      this.api.post('add/negotiation/stage',data)
      .subscribe(res => {
        //console.log("addStageInNegotiation success:" + JSON.stringify(res));
          resolve(res);
        }, (err) => {
         // console.log("addStageInNegotiation error" + JSON.stringify(err))
          reject(err);
        });
      });
  }*/

  createNegotiation(negotiationData){
    return new Promise((resolve, reject) => {
      this.api.post('negotiation',JSON.stringify(negotiationData))
      .subscribe(res => {
        //console.log("createNegotiation success:" + JSON.stringify(res));
        if(res == "Fail"){
          reject(res);
        }else{
          resolve(res);
        }
          
        }, (err) => {
          //console.log("createNegotiation error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  createCardInStageOfNegotiation(cardData){
    return new Promise((resolve, reject) => {
      this.api.post('negotiation/stage/card/create',JSON.stringify(cardData))
      .subscribe(res => {
        //console.log("createNegotiation success:" + JSON.stringify(res));
        if(res == "Fail"){
          reject(res);
        }else{
          resolve(res);
        }
          
        }, (err) => {
          //console.log("createNegotiation error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  startNegotiation(ids){
    return new Promise((resolve, reject) => {
      this.api.post('start/negotiation',JSON.stringify(ids))
      .subscribe(res => {
       // console.log("createNegotiation success:" + JSON.stringify(res));
        if(res == "Fail"){
          reject(res);
        }else{
          resolve(res);
        }
          
        }, (err) => {
          //console.log("createNegotiation error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  assignTeamNegotiation(teamObj){
    return new Promise((resolve, reject) => {
      this.api.post('team/assign',JSON.stringify(teamObj))
      .subscribe(res => {
        //console.log("createNegotiation success:" + JSON.stringify(res));
        if(res == "Fail"){
          reject(res);
        }else{
          resolve(res);
        }
          
        }, (err) => {
          //console.log("createNegotiation error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  getTeamsByNegotiationId(negotiationId:string,canvasId : string){
    // console.log("Requesting Negotiations Details by Negotiation id : "+negotiationId);
     return new Promise((resolve, reject) => {
       this.api.get('team/negotiation/'+negotiationId + '/canvasId/' + canvasId)
       .subscribe(res => {
        // console.log("getAllStagesofNegotiationById success:" + JSON.stringify(res));
           resolve(res);
         }, (err) => {
          // console.log("getAllStagesofNegotiationById error" + JSON.stringify(err))
           reject(err);
         });
       });
   }

   getRunningAndClosedNegotiationDetailsHandler(negotiationId:string,canvasId : string,userId : string){
    // console.log("Requesting Negotiations Details by Negotiation id : "+negotiationId);
     return new Promise((resolve, reject) => {
       this.api.get('negotiations/'+negotiationId + '/canvas/' + canvasId + '/user/' + userId)
       .subscribe(res => {
        // console.log("getAllStagesofNegotiationById success:" + JSON.stringify(res));
           resolve(res);
         }, (err) => {
          // console.log("getAllStagesofNegotiationById error" + JSON.stringify(err))
           reject(err);
         });
       });
   }

   updateNegotiation(negoObj){
    return this.api.put('negotiation/', JSON.stringify(negoObj)).pipe();
  }
}
