import { Injectable } from '@angular/core';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class UserCardsService {

  constructor(private api : ApiService) { }

  getUserCardsByUserId(userId:string,tenantId : string){
    return this.api.get('user/cards/'+userId + '/' + tenantId).pipe();
    // console.log("Requesting Negotiations by user id : "+userId);
    // return new Promise((resolve, reject) => {
    //   this.api.get('user/cards/'+userId + '/' + tenantId)
    //   .subscribe(res => {
    //     console.log("getUserNegotiationsByUserId success:" + JSON.stringify(res));
    //       resolve(res);
    //     }, (err) => {
    //       console.log("getUserNegotiationsByUserId error" + JSON.stringify(err))
    //       reject(err);
    //     });
    //   });
  }

  saveUserCard(cardObj){
    return new Promise((resolve, reject) => {
      this.api.post('save/user/card',JSON.stringify(cardObj))
      .subscribe(res => {
       // console.log("Save Card Success :" + JSON.stringify(res));
        if(res == "Fail"){
          reject(res);
        }else{
          resolve(res);
        }
          
        }, (err) => {
          //console.log("Save Card error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  doneUserCard(cardObj){
    return new Promise((resolve, reject) => {
      this.api.post('done/user/card',JSON.stringify(cardObj))
      .subscribe(res => {
       // console.log("Save Card Success :" + JSON.stringify(res));
        if(res == "Fail"){
          reject(res);
        }else{
          resolve(res);
        }
          
        }, (err) => {
         // console.log("Save Card error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  agreeUserCard(cardObj){
    return new Promise((resolve, reject) => {
      this.api.post('agree/user/card',JSON.stringify(cardObj))
      .subscribe(res => {
       // console.log("Agree Card Success :" + JSON.stringify(res));
        if(res == "Fail"){
          reject(res);
        }else{
          resolve(res);
        }
          
        }, (err) => {
         // console.log("Agree Card error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  disAgreeUserCard(cardObj){
    return new Promise((resolve, reject) => {
      this.api.post('disagree/user/card',JSON.stringify(cardObj))
      .subscribe(res => {
        //console.log("Disagree Card Success :" + JSON.stringify(res));
        if(res == "Fail"){
          reject(res);
        }else{
          resolve(res);
        }
          
        }, (err) => {
         // console.log("Disagree Card error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  moveForwardUserCard(cardObj){
    return new Promise((resolve, reject) => {
      this.api.post('next/card',JSON.stringify(cardObj))
      .subscribe(res => {
        //console.log("Move Forward Success :" + JSON.stringify(res));
        if(res == "Fail"){
          reject(res);
        }else{
          resolve(res);
        }
          
        }, (err) => {
          //console.log("Move Forward error" + JSON.stringify(err))
          reject(err);
        });
      });
  }

  updateCard(cardObj){
    return this.api.put('update/card', JSON.stringify(cardObj)).pipe();

  }

  changeToInprogCard(cardObj) {
    return this.api.put('user/card/status', JSON.stringify(cardObj)).pipe();

  }
  

}
