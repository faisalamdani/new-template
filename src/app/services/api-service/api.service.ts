import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // private url : string = "https://n9g2j2mitl.execute-api.us-east-1.amazonaws.com/Prod";

  //test url
 // private url : string = "https://19vna2mm0k.execute-api.us-east-1.amazonaws.com/Prod";

  //community url
  // private url : string = "https://a2aedruts6.execute-api.us-east-1.amazonaws.com/Prod"

  private url : string = environment.javaApiUrl;

  constructor(private http: HttpClient) { }

  get(endpoint: string) {
    return this.http.get(this.url + '/' + endpoint);
  }

  post(endpoint: string, body: any) {
    return this.http.post(this.url + '/' + endpoint, body);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
  }
}
