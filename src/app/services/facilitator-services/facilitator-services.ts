import { Injectable } from '@angular/core';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class FacilitatorServices {

  constructor(private api : ApiService) { }

  getAllTeamUsersByUserId(userId: string, hostTenantId: string) {
    return this.api.get('team/users/'+userId+'/tenant/'+hostTenantId).pipe();
  }

  assignTeamToNegotiation(data: any){
     return this.api.post('team/assign', JSON.stringify(data)).pipe();
  }
 
  creatTeamToNegotiation(data: any){
    return this.api.post('team', JSON.stringify(data)).pipe();
 }

 deleteTeamUser(teamId, userId){
  return this.api.delete('team/'+teamId+'/user/'+userId).pipe();
 }

}
