import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api-service/api.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  //apiUrl = 'https://ezdvxrtzuc.execute-api.us-east-1.amazonaws.com/production/CognitoAuthenticator';

  constructor(private http: HttpClient, private api: ApiService) { }

  phpUrl = environment.phpApiUrl;

  signIn(data) {
    return this.http.post(this.phpUrl + '/CognitoAuthenticator?action=signin', JSON.stringify(data)).pipe();
  }

  newPasssword(newPasswordData: any) {
    return this.http.post(this.phpUrl + '/CognitoAuthenticator?action=changePassword',JSON.stringify(newPasswordData)).pipe();
  }

  codeVerified(data: any) {
    return this.http.post(this.phpUrl + '/CognitoAuthenticator?action=confirmsignup',JSON.stringify(data)).pipe();
  }

  resendConfirmationCode(data){
    return this.http.post(this.phpUrl + '/CognitoAuthenticator?action=resendConfirmationCode',JSON.stringify(data)).pipe();
  }

  forgotPassword(data){
    return this.http.post(this.phpUrl + '/CognitoAuthenticator?action=forgotPassword',JSON.stringify(data)).pipe();
  }

  confirmforgotPasswordForm(data){
    return this.http.post(this.phpUrl + '/CognitoAuthenticator?action=confirmForgotPassword',JSON.stringify(data)).pipe();
  }

  logOut() {

    let logOutData = {
      accessToken: localStorage.getItem('accessToken')
    }
    if (logOutData != null) {
      return this.http.post(this.phpUrl + '/CognitoAuthenticator?action=SignOut', JSON.stringify(logOutData)).pipe();
    }


  }


  getUserByEmailAndTenant(email: string, tenantId: string) {
    return this.api.get('user/tenant/' + email + "/" + tenantId).pipe();
  }

}
