import { Injectable } from '@angular/core';
import { ApiService } from '../api-service/api.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InviteService {

    //apiUrl = 'https://ezdvxrtzuc.execute-api.us-east-1.amazonaws.com/production/invite';

    constructor(private api : ApiService,private http: HttpClient) { }

    phpUrl = environment.phpApiUrl;
    
    inviteUsers(inviteUsers: any){
      return this.api.post('/invite/user/team',JSON.stringify(inviteUsers)).pipe();
    }

    
    addinviteUsers(inviteUsers : any){
        return new Promise((resolve, reject) => {
          this.http.post(this.phpUrl+'/invite?action=inviteUsers',inviteUsers)
          .subscribe(res => {
            //console.log("getOrchestrationsByTenantId success:" + JSON.stringify(res));
              resolve(res);
            }, (err) => {
              //console.log("getOrchestrationsByTenantId error" + JSON.stringify(err))
              reject(err);
            });
          });
      }

     /* getinviteUsers(email,tenantId){
        return new Promise((resolve, reject) => {
            this.api('orchestrations/tenant/'+tenantId)
            .subscribe(res => {
              //console.log("getOrchestrationsByTenantId success:" + JSON.stringify(res));
                resolve(res);
              }, (err) => {
                //console.log("getOrchestrationsByTenantId error" + JSON.stringify(err))
                reject(err);
              });
            });
      }*/
}