import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CanvasStageDTO } from 'src/app/models/canvasStages';
import { OrchestratorService } from 'src/app/services/orchestration-canvas-service/orchestrator.service';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.scss']
})
export class StageComponent implements OnInit {

  stageStandardNameOptions = [
    { value: 'EXPLORE', label: 'Explore' },
    { value: 'DEFINE', label: 'Define' },
    { value: 'OPTIONS', label: 'Options' },
    { value: 'AGREE', label: 'Agree' },
    { value: 'SETTLE', label: 'Settle' },
  ];

  stageFlowRuleOptions = [
    { value: 'SEQUENTIAL', label: 'Close before start' },
  ]

  stageForm: FormGroup;
  loading = false;
  userdata: any;
  title: string
  stageData: any;
  editStageData: CanvasStageDTO;
  useConsensusQuorumSH: boolean = false;
  canvas: any;
  numberOfStage: string;
  canvasData: any;
  numberOfStages: string;
  action: Subject<any> = new Subject();
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private orchestratorService: OrchestratorService,
    public modalRef: MDBModalRef,
  ) { }

  ngOnInit(): void {

    this.canvas = JSON.parse(this.canvasData);
    this.editStageData = this.stageData;
    if (this.stageData.stageSequenceNumber) {
      this.numberOfStage = this.stageData.stageSequenceNumber
    } else {
      this.numberOfStage = this.numberOfStages;
    }

    const url = window.location.hostname;
    this.userdata = JSON.parse(localStorage.getItem("userdata"));

    this.stageForm = this.formBuilder.group({
      stageId: [''],
      stageName: ['', Validators.required],
      stageStandardName: ['', Validators.required],
      stageDescription: ['', Validators.required],
      sequenceNumber: [this.numberOfStage],
      kanbanType: [this.canvas.kanbanType],
      orchestrationCanvasId: [this.canvas.canvasId],
      orchestrationCanvasName: [this.canvas.orchestrationName],
      stageFlowRule: ['', Validators.required],
      totalCardsInStage: [''],
      orchestratorId: [this.userdata.userId],
      orchestratorName: [this.userdata.firstName + ' ' + this.userdata.lastName],
      hostTenantId: [this.userdata.hostTenantId],
      hostTenantName: [url.split(".", 3)[0]],
    });

    if (this.editStageData) {
      this.stageForm.get('stageId').setValue(this.editStageData.stageId);
      this.stageForm.get('stageName').setValue(this.editStageData.stageName);
      this.stageForm.get('stageStandardName').setValue(this.editStageData.stageStandardName);
      this.stageForm.get('stageDescription').setValue(this.editStageData.stageDescription);
      this.stageForm.get('stageFlowRule').setValue(this.editStageData.stageFlowRule);
    }

  }

  get f() { return this.stageForm.controls; }

  addStage(){

    this.submitted = true;
    if (this.stageForm.invalid) {
      return;
    }

    this.loading = true;

    this.orchestratorService.addStageInOrchestration(this.stageForm.value).subscribe(
      (data: Array<CanvasStageDTO>) => {
        if (this.editStageData) {
          this.action.next("edit");
          this.modalRef.hide();
        } else {
          this.action.next("add");
          this.modalRef.hide();
        }

      },
      error => {
        this.action.next("error");
          this.modalRef.hide();
      }
    );

  }

}
