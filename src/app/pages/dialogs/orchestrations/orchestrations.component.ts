import { Component, OnInit } from '@angular/core';
import { MDBModalRef, ToastService } from 'ng-uikit-pro-standard';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrchestratorService } from 'src/app/services/orchestration-canvas-service/orchestrator.service';
import { OrchestrationCanvasDto } from 'src/app/models/orchestration-canvas';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-orchestrations',
  templateUrl: './orchestrations.component.html',
  styleUrls: ['./orchestrations.component.scss']
})
export class OrchestrationsComponent implements OnInit {

  orchestrationsForm: FormGroup;
  canvasData: any;
  title: string = "";
  userdata: any;
  loading = false;
  submitted = false;
  action: Subject<any> = new Subject();
  options = { positionClass: 'md-toast-bottom-right' };

  KanbanOptions = [
    { value: 'Negotiation', label: 'Negotiation' },
    { value: 'Completion', label: 'Completion' },
  ];

  constructor(
    public modalRef: MDBModalRef,
    private formBuilder: FormBuilder,
    private canvasService: OrchestratorService,
    private toast: ToastService
  ) {

  }

  ngOnInit(): void {

    if (this.canvasData) {
      this.title = "Edit Orchestration";
    } else {
      this.title = "Add Orchestration";
    }

    this.userdata = JSON.parse(localStorage.getItem("userdata"));

    this.orchestrationsForm = this.formBuilder.group({
      canvasId: [''],
      hostTenantId: [this.userdata.hostTenantId],
      orchestrationName: ['', Validators.required],
      orchestrationDescription: ['', Validators.required],
      kanbanType: ['', Validators.required],
      orchestratorId: [this.userdata.userId],
      orchestratorFirstName: [this.userdata.firstName],
      orchestratorLastName: [this.userdata.lastName],
      createdDateTime: [''],
    });

    if (this.canvasData) {
      this.orchestrationsForm.controls['canvasId'].setValue(this.canvasData.canvasId);
      this.orchestrationsForm.controls['orchestrationName'].setValue(this.canvasData.orchestrationName);
      this.orchestrationsForm.controls['kanbanType'].setValue(this.canvasData.kanbanType);
      this.orchestrationsForm.controls['orchestrationDescription'].setValue(this.canvasData.orchestrationDescription);
    }

  }

  get f() { return this.orchestrationsForm.controls; }

  addOrchestrations() {

    this.submitted = true;

    if (this.orchestrationsForm.invalid) {
      return;
    }

    this.loading = true;
    this.toast.info('Processing...', '', this.options)
    this.canvasService.createOrchestrationCanvas(this.orchestrationsForm.value).subscribe(
      (data: Array<OrchestrationCanvasDto>) => {
        if (this.canvasData) {
          this.action.next("edit");
          this.modalRef.hide();
        } else {
          this.action.next("add");
          this.modalRef.hide();
        }

      },
      error => {
        this.action.next('error');
        this.modalRef.hide();
      }
    );

  }

}
