import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrchestrationsComponent } from './orchestrations.component';

describe('OrchestrationsComponent', () => {
  let component: OrchestrationsComponent;
  let fixture: ComponentFixture<OrchestrationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrchestrationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrchestrationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
