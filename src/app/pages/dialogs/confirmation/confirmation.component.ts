import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  action: Subject<any> = new Subject();

  constructor(public modalRef: MDBModalRef) { }

  ngOnInit(): void {
  }

  onYesClick() {
    this.action.next('Yes');
    this.modalRef.hide();
  }

  onNoClick() {
    this.action.next('No');
    this.modalRef.hide();
  }

}
