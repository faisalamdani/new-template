import { Component, OnInit,Input,Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';
import {UserModel} from './../../models/user-model.model';

@Component({
  selector: 'app-card-grid',
  templateUrl: './card-grid.component.html',
  styleUrls: ['./card-grid.component.scss']
})
export class CardGridComponent implements OnInit {

  @Input() cardsArr :Array<UserModel> =[];
  @Input() filterData = [];
  @Output() removeFilerItem = new EventEmitter<{Arr:any[], item:any}>(); 
 
 
   constructor(private route : Router) { }
  
   // userArr : any = ["Amit","Rahul","Raj"];
  
   p: number = 1;
   visible = true;
   selectable = true;
   removable = true;
 
   ngOnInit() {
     //console.log(this.filterData);
   }
 
   editCard(item){
     let cardObj :any = {};
     cardObj.negotiationId = item.negotiationId;
     cardObj.cardId = item.cardId;
     cardObj.canvasId = item.orchestrationCanvasId;
     //console.log(item);
     this.route.navigate(['/interplay/card-edit/'],{queryParams : cardObj});
 
   }
 
   remove(item): void {
     //console.log(item)
     const index = this.filterData.indexOf(item);
 
     if (index >= 0) {
       this.filterData.splice(index, 1);
     }
 
     this.removeFilerItem.emit({Arr :this.filterData,item :item}); 
 
 
   }
   
 }
 