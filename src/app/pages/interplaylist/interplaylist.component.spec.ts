import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterplaylistComponent } from './interplaylist.component';

describe('InterplaylistComponent', () => {
  let component: InterplaylistComponent;
  let fixture: ComponentFixture<InterplaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterplaylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterplaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
