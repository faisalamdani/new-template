import { Component, OnInit,Input } from '@angular/core';
import { NegotiationService } from 'src/app/services/negotiations-services/negotiation.service';
import { OrchestratorService } from 'src/app/services/orchestration-canvas-service/orchestrator.service';
import { OrchestrationCanvasDto } from 'src/app/models/orchestration-canvas';
import { NegotiationDto } from 'src/app/models/negotiation';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-interplaylist',
  templateUrl: './interplaylist.component.html',
  styleUrls: ['./interplaylist.component.scss']
})
export class InterplaylistComponent implements OnInit {
  loading = true;
  state: any;
  p: number = 1;
  canvasId: string;
  negotiationList: Array<NegotiationDto> = [];
  orchestrationArr: Array<OrchestrationCanvasDto> = new Array<OrchestrationCanvasDto>();

  userdata: any;
  canvasData: any;
  addNegoBtnPer: boolean = false;

  @Input() shadows = true;

  private sorted = false;

  constructor( 
    private negotiationService: NegotiationService,
    private router: Router,
    private route: ActivatedRoute,
 
    private orchService: OrchestratorService
  ) {

    this.userdata = JSON.parse(localStorage.getItem("userdata"));
  }

  ngOnInit() {
    if(this.userdata){
      this.getAllNegotiationsByUserId();
    }
    if(this.userdata.role != 'PARTICIPANT')
    {
      this.orchService.getOrchestrationsByTenantId(this.userdata.hostTenantId).subscribe(
        (data: Array<OrchestrationCanvasDto>) => {
          this.orchestrationArr = data;
        },
        error => {
          //this.error = error
        }
      );
    }
  }

  getAllNegotiationsByUserId() {
    this.negotiationService.getAllNegotiationsByUserId(this.userdata.userId,this.userdata.hostTenantId)
      .then((data: Array<NegotiationDto>) => {
        this.negotiationList = data;
        this.loading = false;
        console.log(this.negotiationList);
        this.negotiationList = this.negotiationList.sort((a: any, b: any) =>
        new Date(b.updatedTime).getTime() - new Date(a.updatedTime).getTime()
    );
      }).catch((err) => {
        //alert("Please try again : reason :" + err);
      })
  }

  sortBy(by: string | any): void {

    this.negotiationList.sort((a: any, b: any) => {
      if (a[by] < b[by]) {
        return this.sorted ? 1 : -1;
      }
      if (a[by] > b[by]) {
        return this.sorted ? -1 : 1;
      }

      return 0;
    });

    this.sorted = !this.sorted;
  }

  goToCreateCard(item) {
    let obj: any = {};
    //console.log(item);
    obj.canvasId = item.canvasId;
    obj.orchestrationName = item.orchestrationName
    // this.route.navigate(["/facilitator-card/", quw);
    this.router.navigate(['/interplay/card-edit/'], { queryParams: obj });
  }

  viewNego(negotiation) {
    this.router.navigate(["/interplay/card-edit/" + negotiation.negotiationId + "/" + negotiation.canvasId]);
  }

  viewCards(negotiation,cardView) {
    this.router.navigate(["/interplay/cards/" + negotiation.negotiationId + "/" + cardView]);  
  }
}

