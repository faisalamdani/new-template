import { Component, OnInit } from '@angular/core';
import { CardStructure, TriggerCardDTO } from 'src/app/models/trigger-card-dto';
import { UserNegotiation } from 'src/app/models/userNegotiation';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { field, value } from 'src/app/models/cardDragDrop';
import { OrderPipe } from 'ngx-order-pipe';
import { CardsDTO } from 'src/app/models/card';
import { KeyValue } from '@angular/common';
import { CanvasDetailsDTO } from 'src/app/models/canvasDetails';
import { ActivatedRoute } from '@angular/router';
import { OrchestratorService } from 'src/app/services/orchestration-canvas-service/orchestrator.service';
import { User } from 'src/app/models/user';
import { NegotiationService } from 'src/app/services/negotiations-services/negotiation.service';
import { UserCardsService } from 'src/app/services/user/user-cards.service';

@Component({
  selector: 'app-card-edit',
  templateUrl: './card-edit.component.html',
  styleUrls: ['./card-edit.component.scss']
})
export class CardEditComponent implements OnInit {
  item: any = {};
  showDoneBtn = true;
  showDate = false;
  screenHeight: number;
  showNotificationDate = false;
  showToggle = false;
  showDueDate = false;
  user: User;
  card: CardsDTO;
  cardStructureArr: Array<CardStructure> = [];
  cardObj: TriggerCardDTO = new TriggerCardDTO();
  showShareOpt = false;
  showDateToggle = true;
  showBeforeInstruction = false;
  disableComment = false;
  comment = "";
  showComment = false;
  stageId = "";
  dueDate;
  notificationDate;
  dueHrs = 12;
  notficationHrs: number = 1;
  notficationMin: number = 0;
  dueMin = 0;
  timeZone = "PM";
  notificationTimeZone = "PM";
  fromInterPlay = false;
  shareBtn = false;
  disabledForm = true;
  showCreateNego = true;
  teamObj: Array<User>;
  showNegoEdit = false;
  showShedule = false;
  showNegoDescEdit = false;
  showAddCategory = false;
  showStartNego = false;
  categoryName: any;
  showSave = false;
  showDoneCard = false;
  useDueDate = false;
  useNotificationDate = false;
  showFullForm = false;
  cardName: any;
  cardId: any = "";
  active: any = '';
  showFormTable = false;
  stageIndex;
  cardIndex;
  expandFormCard = false;
  showLibrary = true;
  orchestrationName: any = "";
  negotiationName: any;
  templateName = "";
  negotiationDesc: any = ""
  loading = true;
  canvasId: any;
  negotiationId: any;
  modelFields: Array<field> = [];
  stageArr: any = [];
  model: any = {
    attributes: this.modelFields
  };
  teamName: string = "";
  isActive: string;
  lastCardName: string = "";


  isDisabled: boolean;

  value: value = {
    label: "",
    value: ""
  };

  barthemeArr: Array<field> = [
    {
      "type": "rating",
      "icon": "fa-text-width",
      "label": "Rating-star",
      "theme": "stars",
      "max": 5,
      "value": 5
    },
    {
      "type": "rating",
      "icon": "fa-text-width",
      "label": "Rating-Number",
      "theme": "square",
      "max": 5,
      "value": 5
    },
  ];

  fieldModels: Array<field> = [
    {
      "type": "text",
      "icon": "fa-address-card-o",
      "label": "Text Field",
      "regex": "",
      "value": ""
    },
    {
      "type": "textarea",
      "icon": "fa-cc",
      "label": "Textarea",
      "value": ""
    },
    {
      "type": "checkbox",
      "required": true,
      "label": "Checkbox",
      "icon": "fa-list",
      "inline": true,
      "values": [
        {
          "label": "Option 1",
          "value": "option-1"
        },
        {
          "label": "Option 2",
          "value": "option-2"
        }
      ]
    },
    {
      "type": "number",
      "label": "Number",
      "icon": "fa-object-group",
      "min": 12,
      "max": 90,
      "value": ""
    }
  ];

  toppingList: string[] = ['Category 1', 'Category 2', 'Category 3', 'Category 4'];
  selected = -1;

  /*checkbox change event*/
  onChange(event) {
    //console.log(event)
  }

  constructor(
    private route: ActivatedRoute,
    private canvasService: OrchestratorService,
    // private dialog: MatDialog,
    // private toast: ToastService,
    private orderPipe: OrderPipe,
    private negoService: NegotiationService,
    private userService: UserCardsService
  ) {
  }




  ngOnInit() {

    this.user = JSON.parse(localStorage.getItem("userdata"));

    this.route.queryParams.subscribe(params => {
      if (params.hasOwnProperty('negotiationId')) {
        localStorage.setItem('negotiationId', params.negotiationId);
        this.negotiationId = params.negotiationId
        this.cardId = params.cardId;
        this.canvasId = params.canvasId;
      }
      else if (params.hasOwnProperty('canvasId')) {
        this.orchestrationName = params.orchestrationName;
        this.orchestrationName = this.orchestrationName;
        this.canvasId = params.canvasId;
        this.negotiationId = localStorage.getItem('negotiationId');
      }
      else {
        this.negotiationId = this.route.snapshot.paramMap.get('negotiationId');
        this.canvasId = this.route.snapshot.paramMap.get('canvasId');
        localStorage.setItem('negotiationId', this.negotiationId);
        this.fromInterPlay = true;
      }
    });
    // console.log(this.negotiationId);
    //console.log(this.canvasId);
    if (this.negotiationId === "" || this.negotiationId === "undefined" || this.negotiationId === null) {
      //console.log("orch");
      this.showFullForm = true
      this.getStageDetailsByOrchestration();
    }

    else if (this.cardId != "") {
      this.showCreateNego = false;
      this.showFullForm = false;
      //console.log("card")
      this.getRunningAndClosedNegotiationDetailsHandler();
    }

    else {
      this.shareBtn = true;
      this.showShareOpt = true;
      this.showFullForm = true;
      this.showCreateNego = false;
      //console.log("nego");
      this.getStageDetailsByNegotiation();
    }
  }

  addValue(values) {
    values.push(this.value);
    this.value = { label: "", value: "" };
  }
  getStageDetailsByOrchestration() {
    this.canvasService.getAllStagesInOrchestration(this.canvasId, this.user.hostTenantId).subscribe(
      (data: CanvasDetailsDTO) => {
        //console.log(data.stages);
        this.stageArr = this.orderPipe.transform(data.stages, 'stageSequenceNumber', false);
        this.checkEditRule();
        this.orchestrationName = data.orchestrationCanvasName;
        this.templateName = "Template of " + this.orchestrationName;
        let date = new Date().toLocaleDateString();
        this.showCardForm(this.stageArr[0].cards[0], 0, 0);
        // console.log(date);
        this.negotiationName = '';
        this.loading = false;
      },
      error => {
      }
    );
  }

  disagree() {
    this.loading = true;
    this.userService.disAgreeUserCard(this.assignCardForAgree()).then(success => {
      this.disableComment = true;
      this.disabledForm = true;
      setTimeout(() => {
        // console.log('after');
        this.getRunningAndClosedNegotiationDetailsHandler();
      }, 2500);
      this.stageArr[this.stageIndex].cards[this.cardIndex].comment = this.comment;

    }).catch(err => {

    });
  }

  agree() {
    this.loading = true;
    this.userService.agreeUserCard(this.assignCardForAgree()).then(success => {
      this.disableComment = true;
      this.disabledForm = true;
      setTimeout(() => {
        // console.log('after');
        this.getRunningAndClosedNegotiationDetailsHandler();
      }, 2500);
      this.stageArr[this.stageIndex].cards[this.cardIndex].comment = this.comment;

    }).catch(err => {

    });
  }

  assignCardForAgree() {
    let cardObj: any = {};
    cardObj.cardId = this.card.cardId;
    cardObj.userId = this.user.userId;
    cardObj.comment = this.comment;
    let cardStrucObj: CardStructure = new CardStructure();
    cardObj.cardStructure = [];
    this.card.cardStructure.forEach((item: any) => {
      cardStrucObj = item;
      cardObj.cardStructure.push(cardStrucObj);
    })

    return cardObj;
  }
  getStageDetailsByNegotiation() {
    this.negotiationId = localStorage.getItem('negotiationId')

    this.canvasService.getAllStagesInNegotiation(this.negotiationId, this.canvasId).subscribe(
      (data: UserNegotiation) => {
        if (data.stages) {
          let lastCardData = data.stages[data.stages.length - 1].cards[data.stages[data.stages.length - 1].cards.length - 1];
          this.lastCardName = "Card: " + lastCardData.stageSequenceNumber + "." + lastCardData.cardSequenceNumber + " " + lastCardData.cardName;
        }
        this.stageArr = this.orderPipe.transform(data.stages, 'stageSequenceNumber', false);
        this.checkEditRule();
        this.orchestrationName = data.orchestrationCanvasName;
        let date = new Date().toDateString();
        this.negotiationName = data.negotiationName;
        this.negotiationDesc = data.negotiationDescription;

        if (this.fromInterPlay) {
          if (data.negotiationStatus === 'CLOSED')
            this.templateName = "Closed InterPlay: " + this.negotiationName;

          else
            this.templateName = "Edit InterPlay: " + this.negotiationName;
        }
        else {
          if (data.negotiationStatus === 'CLOSED')
            this.templateName = "InterPlay-as-actioned: " + this.negotiationName;

          else
            this.templateName = "InterPlay-in-action: " + this.negotiationName;
        }

        this.teamName = data.teamName;
        if (data.negotiationStatus === "NOT_STARTED")
          this.showStartNego = true;
        else {
          this.showStartNego = false;
          this.showShareOpt = false;

        }

        if (data.users.length > 0) {
          this.teamName = data.teamName;
          this.showFullForm = false;
          this.teamObj = data.users;
          this.shareBtn = false;
          //console.log(this.teamObj)

          //  this.showCreateNego = false;
        }
        else {
          this.showStartNego = false;

        }
        // if(data.users.length <= 0)
        // {
        //   this.showStartNego = false;
        //   this.shareBtn = true;
        // }




        let cardObj;
        let stageIndex;
        let cardIndex;
        if (this.cardId != "") {
          this.stageArr.forEach((element, i) => {
            element.cards.find((item, j) => {
              if (item.cardId === this.cardId) {
                cardObj = item;
                stageIndex = i;
                cardIndex = j;
              }
            });
          });
          //console.log(cardObj);
          //console.log(this.user);
          //  if(this.user.role === 'PARTICIPANT'){
          //   let stageArr =  this.stageArr[this.stageIndex].cards.filter(item => item.cardStatus === 'COMPLETE');
          //   if(cardObj.cardStatus != 'COMPLETE')
          //   stageArr.cards.push(cardObj);
          //   this.stageArr = stageArr;
          // }
          // else{
          //   let stageArr = this.stageArr.filter(stage => {
          //      stage.cards.filter(card => card.cardStatus != "NOT_STARTED")
          //   });
          //   this.stageArr = stageArr;
          //   console.log(this.stageArr);
          // }
          this.showCardForm(cardObj, stageIndex, cardIndex);
        }
        else{
          this.showCardForm(this.stageArr[0].cards[0], 0, 0);

        }
        // this.getTeam(); 
        // console.log(this.stageArr);  
        this.loading = false;
      },
      error => {
      }
    );
  }

  updateNego() {
    this.negotiationId = localStorage.getItem('negotiationId')
    //console.log(this.canvasId);
    if (this.negotiationId != null) {
      let negoObj: any = {};
      negoObj.canvasId = this.canvasId;
      negoObj.negotiationId = this.negotiationId;
      negoObj.negotiationName = this.negotiationName;
      negoObj.negotiationDescription = this.negotiationDesc;




      // console.log(negoObj);
      this.negoService.updateNegotiation(negoObj).subscribe(success => {
        // console.log(success);
      }, err => {
        // console.log(err);
      })
    }
  }

  createNegotiation() {
    this.loading = true;
    //console.log("create negotiation");
    let obj: any = {};
    let user = JSON.parse(localStorage.getItem("userdata"));
    this.user = user;
    //console.log(user);
    obj.userId = user.userId;
    obj.userRole = user.role;
    obj.canvasId = this.canvasId;
    obj.negotiationName = this.negotiationName;
    obj.negotiationDescription = this.negotiationDesc;
    obj.hostTenantId = user.hostTenantId;
    obj.hostTenantName = user.hostTenantName;

    this.negoService.createNegotiation(obj).then(
      (data: UserNegotiation) => {
        this.shareBtn = true;
        // console.log(data);
        this.stageArr = this.orderPipe.transform(data.stages, 'stageSequenceNumber', false);
        //console.log(this.stageArr);
        this.checkEditRule();
        this.orchestrationName = data.orchestrationCanvasName;
        this.negotiationId = data.negotiationId;
        localStorage.setItem('negotiationId', this.negotiationId);
        //let date = new Date().toDateString();
        // console.log(date);
        this.negotiationName = data.negotiationName;
        this.negotiationDesc = data.negotiationDescription;

        if (this.fromInterPlay) {
          if (data.negotiationStatus === 'CLOSED')
            this.templateName = "Closed InterPlay: " + this.negotiationName;

          else
            this.templateName = "Edit InterPlay: " + this.negotiationName;
        }
        else {
          if (data.negotiationStatus === 'CLOSED')
            this.templateName = "InterPlay-as-actioned: " + this.negotiationName;

          else
            this.templateName = "InterPlay-in-action: " + this.negotiationName;
        }

        this.showShareOpt = true;
        this.showFullForm = true;
        // this.saveCardForm();
        // let cardObj;
        //    let stageIndex;
        //    let cardIndex;
        //    console.log(this.cardId);
        //   if(this.cardId != "")
        //   {
        //     this.stageArr.forEach((element,i) => {
        //       element.cards.find((item,j) => {
        //         if(item.cardId === this.cardId)
        //           {
        //             cardObj = item;
        //             stageIndex = i;
        //             cardIndex = j;
        //           }
        //       });
        //    });
        //    console.log(cardObj);
        //    console.log(this.user);
        //    if(this.user.role === 'PARTICIPANT'){
        //     let stageArr =  this.stageArr[this.stageIndex].cards.filter(item => item.cardStatus === 'COMPLETE');
        //     if(cardObj.cardStatus != 'COMPLETE')
        //     stageArr.cards.push(cardObj);
        //     this.stageArr = stageArr;
        //   }
        //    this.showCardForm(cardObj,stageIndex,cardIndex);
        //   }


        // this.toast.showSnackBar("Successfully Negotiation Created");
        this.showCreateNego = false;
        this.showFullForm = true;
        this.loading = false;
        // this.shareCardToUser('new');
      }).catch(err => {
        //console.log(err);
        // this.toast.showSnackBar("Something went wrong");
      });
  }

  selectDate(event) {
    //console.log(event);
    this.stageArr[this.stageIndex].cards[this.cardIndex].dueDate = this.getTimeStamp().toString();
    this.updateCard();
  }

  selectnotificationDate(event) {
    this.stageArr[this.stageIndex].cards[this.cardIndex].notifiedDate = this.getNotficationTimeStamp().toString();
    this.updateCard();
  }

  selectTime(event) {
    this.stageArr[this.stageIndex].cards[this.cardIndex].dueDate = this.getTimeStamp().toString();
    this.updateCard();

  }
  selectMin(event) {
    this.stageArr[this.stageIndex].cards[this.cardIndex].dueDate = this.getTimeStamp().toString();
    this.updateCard();

  }


  selectNotificationTime(event) {
    this.stageArr[this.stageIndex].cards[this.cardIndex].notifiedDate = this.getNotficationTimeStamp().toString();
    this.updateCard();
  }

  selectNotificationMin(event) {
    this.stageArr[this.stageIndex].cards[this.cardIndex].notifiedDate = this.getNotficationTimeStamp().toString();
    this.updateCard();
  }

  selectTimeZone(event) {
    this.stageArr[this.stageIndex].cards[this.cardIndex].dueDate = this.getTimeStamp().toString();
    this.updateCard();

  }

  selectNotificationTimeZone(event) {
    this.stageArr[this.stageIndex].cards[this.cardIndex].notifiedDate = this.getNotficationTimeStamp().toString();
    this.updateCard();
  }

  getNotficationTimeStamp() {
    let d = new Date(this.notificationDate);
    //console.log(d);
    (this.notificationTimeZone === "PM") ? d.setHours(this.notficationHrs + 12) : d.setHours(this.notficationHrs);
    d.setMinutes(this.notficationMin);

    let timestamp = d.getTime();
    
    // console.log(timestamp);
    return timestamp;
  }

  getTimeStamp() {
    let d = new Date(this.dueDate);
    //console.log(d);
    (this.timeZone === "PM") ? d.setHours(this.dueHrs + 12) : d.setHours(this.dueHrs);
    d.setMinutes(this.dueMin);
    // console.log(d.getDate())
    // let timestamp = Date.UTC(d.getFullYear(),d.getMonth(),d.getDate(),d.getHours());
    // console.log(d.getDate())
    // console.log(d.getHours())
    let timestamp = d.getTime();
    // console.log(timestamp);
    return timestamp;
  }
  setDueDate() {

  }

  bothSlide(event) {
    if (event.checked == true) {
      this.useNotificationDate = true;
      this.useDueDate = true;
      this.showNotificationDate = true;
      this.showDate = true;
    } else {
      this.useNotificationDate = false;
      this.useDueDate = false;
      this.showNotificationDate = false;
      this.showDate = false;
    }
    this.stageArr[this.stageIndex].cards[this.cardIndex].useDueDate = this.useDueDate;
    this.stageArr[this.stageIndex].cards[this.cardIndex].useNotification = this.useNotificationDate;
    this.updateCard();
  }

  dateSlide(event) {
    if (event.checked == true) {
      this.showDate = !this.showDate;
      this.useDueDate = !this.useDueDate;
      this.showNotificationDate = !this.showNotificationDate;
    }else{
      this.showDate = false;
      this.useDueDate = false;
      this.showNotificationDate = false;
      this.useNotificationDate = false;
    }
    //console.log(this.showDate);
    this.stageArr[this.stageIndex].cards[this.cardIndex].useDueDate = this.useDueDate;
    this.stageArr[this.stageIndex].cards[this.cardIndex].useNotification = this.useNotificationDate;
    this.updateCard();
  }

  notDateSlide() {
    this.showNotificationDate = !this.showNotificationDate;
    this.useNotificationDate = !this.useNotificationDate;
    this.stageArr[this.stageIndex].cards[this.cardIndex].useNotification = this.useNotificationDate;
    if(this.useNotificationDate)
    this.stageArr[this.stageIndex].cards[this.cardIndex].notifiedDate = this.getNotficationTimeStamp().toString();

    this.updateCard();
  }
  updateCard() {
    let cardObj: any = {};
    cardObj.negotiationId = this.negotiationId;
    cardObj.stageId = this.stageArr[this.stageIndex].stageId;
    cardObj.cardId = this.card.cardId;
    cardObj.useDueDate = this.useDueDate;
    if (this.useDueDate) {
      cardObj.dueDate = this.getTimeStamp();

    }
    else
      cardObj.dueDate = "";

    cardObj.useNotification = this.useNotificationDate;
    if (this.useNotificationDate) {
      cardObj.notifiedDate = this.getNotficationTimeStamp();

    }
    else
      cardObj.notifiedDate = "";
    // console.log(new Date(cardObj.notifiedDate));
    // console.log(new Date(cardObj.dueDate));
    // console.log(cardObj);
    this.userService.updateCard(cardObj).subscribe(success => {
      //console.log(success);
    },
      err => {
        //console.log(err);
      })
  }

  showCardForm(card, i, j) {
    this.isActive = card.cardId;
    this.showDueDate = true;
    console.log(card);
    // console.log(this.stageArr[i].stageId);
    this.active = card;
    this.comment = "";
    //  if(this.cardId != '' && card.cardStatus === 'NOT_STARTED')
    //  {
    //     let cardObj :any = {};
    //     cardObj.cardId = card.cardId;
    //     cardObj.userId = this.user.userId;
    //     console.log(cardObj);
    //     this.userService.changeToInprogCard(cardObj).subscribe(success => {
    //       console.log(success);
    //       this.stageArr[i].cards[j].cardStatus = 'IN_PROGRESS';
    //       this.getRunningAndClosedNegotiationDetailsHandler();
    //       this.showCardForm(card,i,j);
    //       this.loading = false;
    //     },err => {

    //     })
    //  }

    if (card.comment) {
      this.comment = card.comment;
      this.disableComment = true;
    }

    this.useDueDate = card.useDueDate;
    this.useNotificationDate = card.useNotification;
    if (this.user.role === 'FACILITATOR' && (card.cardStatus === 'IN_PROGRESS' || card.cardStatus === 'NOT_STARTED') && this.cardId === "") {
      this.showToggle = true;
    }
    else {
      this.showToggle = false;
    }
    if (card.stateStandardName === 'AGREE')
      this.showComment = true;
    else
      this.showComment = false;
    if ((card.stateStandardName === 'AGREE' && card.cardStatus === 'CLOSED') || (card.stateStandardName === 'AGREE' && card.cardStatus === 'DONE'))
      this.disableComment = true;
    if (this.useDueDate) {
      this.showDate = true;
      this.dueDate = new Date(JSON.parse(card.dueDate));
      //  this.dueDate = new Date(card.dueDate);
      // console.log(this.dueDate);
      let hours = this.dueDate.getHours();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12;
      this.dueHrs = hours;
      this.timeZone = ampm;
      this.dueMin = this.dueDate.getMinutes();
    }
    else {
      this.showDate = false;
    }

    if (this.useNotificationDate) {
      this.showNotificationDate = true;
      this.notificationDate = new Date(JSON.parse(card.notifiedDate));
      //  this.dueDate = new Date(card.dueDate);
      // console.log(new Date(JSON.parse(card.notifiedDate)));
      let hours = this.notificationDate.getHours();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12;
      this.notficationHrs = hours;
      this.notificationTimeZone = ampm;
      this.notficationMin = this.notificationDate.getMinutes();

    }
    else {
      this.showNotificationDate = false;
    }

    this.disabledForm = false;
    this.card = card;
    this.loading = false;
    this.showSave = true;
    this.stageIndex = i;

    this.cardIndex = j;


    // this.model.attributes = card.cardStructure;
    // let cardStructure:any = {};
    // cardStructure.cardStructure : Array<field> = card.cardStructure;
    // console.log(card.cardStructure);
    if (this.cardId != "") {
      //console.log("from card grid")
      card.cardStructure.forEach(element => {
        element.cardStructure.forEach(item => {
          item.toggle = false;
        });
      });
      this.cardStructureArr = card.cardStructure;
    }
    else {
      // console.log("from nego list");
      //console.log(card.cardStructure);
      card.cardStructure.forEach(element => {

        element.toggle = false;
      });
      // this.cardId = card.cardId;
      let cardStructureObj: CardStructure = new CardStructure();
      cardStructureObj.cardStructure = card.cardStructure;
      this.cardStructureArr[0] = cardStructureObj;
    }
    // console.log(this.cardStructureArr);

    // console.log(this.model)
    this.model.cardName = card.cardName;
    if (card.cardStatus === 'NOT_STARTED' || card.cardStatus === "") {
      this.disabledForm = true;
      this.showLibrary = false;
      // this.showFullForm = true;
    }
    else {
      // this.disabledForm = false;
      this.showLibrary = true;
      this.disabledForm = this.checkCardRules(card.cardRights, card.cardStatus);
      this.showDoneCard = !this.disabledForm;
    }
    if (this.fromInterPlay) {
      this.disabledForm = true;
      this.showDoneCard = false;
    }
    else {
      // if(((card.userRoleAssigned === 'FACILITATOR' && this.cardStructureArr[0].userId) || (card.stateStandardName === 'AGREE' && this.cardStructureArr[0].userId)) && card.dataManagementRule.structureView === 'TABLE')

      if (card.dataManagementRule.structureView === 'TABLE') {
        this.showFormTable = true;
      }
      else {
        this.showFormTable = false;
      }
    }

  }

  saveCard() {
    let negotiationId = localStorage.getItem('negotiationId');
    this.negotiationId = negotiationId;
    if (negotiationId === "" || negotiationId === "undefined")
      this.createNegotiation();
    else
      this.saveCardForm();

  }

  valueAscOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return a.value.localeCompare(b.value);
  }

  indexOrderAsc = (akv: KeyValue<string, any>, bkv: KeyValue<string, any>): number => {
    const a = akv.value.sno;
    const b = bkv.value.sno;

    return a > b ? 1 : (b > a ? -1 : 0);
  };

  saveCardForm() {
    this.stageArr[this.stageIndex].cards[this.cardIndex].cardName = this.model.cardName;
    let obj: any = {};
    obj = this.model.attributes;
    this.stageArr[this.stageIndex].cards[this.cardIndex].cardStructure = obj;
    let obj1: any = {};
    obj1.stageId = this.stageArr[this.stageIndex].stageId;
    let negotiationId = localStorage.getItem('negotiationId');
    this.negotiationId = negotiationId;
    obj1.negotiationId = negotiationId;
    let cardObj: CardsDTO = this.stageArr[this.stageIndex].cards[this.cardIndex];
    obj1.card = cardObj;
    // console.log(obj1);
    // this.negoService.createCardInStageOfNegotiation(obj1).then(success => {
    //   console.log(success);
    // }).catch(err => {
    //   console.log(err);
    // })
  }

  removeField(i) {
    this.model.attributes.splice(i, 1);
  }

  drop(event) {
    // console.log(event);
    let obj: field = {};
    Object.assign(obj, event);
    // console.log(obj);
    this.model.attributes.push(obj);
    //console.log(this.model);
  }

  dropList(event: CdkDragDrop<string[]>) {
    // console.log(event.previousIndex + "-" + event.currentIndex)
    moveItemInArray(this.model.attributes, event.previousIndex, event.currentIndex);
    // console.log(this.model);

  }

  dropRatings(event) {
    // console.log(event);
    let obj: field = {};
    // Object.assign(obj, event);
    this.model.attributes.push(obj);
    // console.log(this.model);
  }

  // Share Modal
  // shareCardToUser(data) {

  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.disableClose = true;
  //   dialogConfig.width = '60%';
  //   dialogConfig.height = '500px';

  //   dialogConfig.data = {
  //     title: 'User Share Card',
  //     canvasId: this.canvasId,
  //     negotiationId: this.negotiationId,
  //     data: data
  //   };

  //   const dialogRef = this.dialog.open(ShareCardToUserDialogComponent, dialogConfig);

  //   dialogRef.afterClosed().subscribe(
  //     (data: any) => {

  //       if (data == "create") {
  //         this.shareCardToUser(data);
  //       } else if (data == "error") {
  //         this.toast.showSnackBar("Something went wrong");
  //       } if (data == "close") {

  //       }
  //       else {
  //         this.teamObj = data.users;

  //         // this.teamObj.canvasId = this.canvasId;
  //         // this.teamObj.negotiationId = this.negotiationId;
  //         //this.negoService.assignTeamNegotiation(this.teamObj);
  //         this.showStartNego = true;
  //         this.shareBtn = false;
  //         this.showShedule = true;
  //         this.showFullForm = false;
  //       }


  //     }
  //   );
  // }

  save(item, topping) {
    // console.log(item);
    // console.log(topping)
  }

  ngOnDestroy() {
    //console.log("leaving page");
    localStorage.removeItem('negotiationId');
  }

  // gotoSchedule() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.disableClose = true;
  //   dialogConfig.width = '60%';

  //   dialogConfig.data = {
  //     title: 'Schedule',
  //     //cardData: cardData
  //   };
  //   const dialogRef = this.dialog.open(SheduleDialogComponent, dialogConfig);

  //   dialogRef.afterClosed().subscribe(

  //   );
  // }

  moveForward(card) {
    this.loading = true;
    let cardObj: any = {};
    cardObj.cardId = card.cardId;
    this.userService.moveForwardUserCard(cardObj).then(success => {
      // console.log(success);
      setTimeout(() => {
        this.getRunningAndClosedNegotiationDetailsHandler();
      }, 2500);

    }).catch(err => {
      // console.log(err);

    });
  }

  getRunningAndClosedNegotiationDetailsHandler() {
    this.loading = true;
    //console.log('from card');
    //console.log(this.canvasId);
    this.negoService.getRunningAndClosedNegotiationDetailsHandler(this.negotiationId, this.canvasId, this.user.userId).then((data: UserNegotiation) => {
      if (data.stages) {
        let lastCardData = data.stages[data.stages.length - 1].cards[data.stages[data.stages.length - 1].cards.length - 1];
        this.lastCardName = "Card: " + lastCardData.stageSequenceNumber + "." + lastCardData.cardSequenceNumber + " " + lastCardData.cardName;
      }
      this.teamName = data.teamName;
      this.assignData(data);
    }).catch(err => {

    })
  }

  // Team Modal
  // gotoTeam() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.disableClose = true;
  //   dialogConfig.width = '25%';
  //   dialogConfig.height = '72%';
  //   dialogConfig.position = {
  //     right: '0px'
  //   };
  //   dialogConfig.data = {
  //     title: 'Schedule',
  //     teamData: this.teamObj,
  //     teamName: this.teamName,
  //     cardName: this.lastCardName
  //   };
  //   const dialogRef = this.dialog.open(TeamDialogComponent, dialogConfig);

  //   dialogRef.afterClosed().subscribe(

  //   );
  // }

  getTeam() {
    this.negoService.getTeamsByNegotiationId(this.negotiationId, this.canvasId).then(success => {
      // console.log(success);
    }).catch(err => {
      // console.log(err);
    });
  }

  checkCardRules(ruleArr, cardStatus) {
    this.isDisabled = true;

    ruleArr.forEach(ruleObj => {
      if (this.user.role === ruleObj.privacyRule) {
        if (ruleObj.permissionType === "EDIT_CARD") {
          if (ruleObj.stateRule === cardStatus) {
            if (cardStatus === "IN_PROGRESS") {
              this.isDisabled = false;
            }
          }
        }
        else if (ruleObj.permissionType === "REVIEW_CARD") {
          if (ruleObj.stateRule === cardStatus) {
            if (cardStatus === "DONE_OR_COMPLETE") {
              this.isDisabled = true;
            }
          }
        }
      }
      else {
        if (ruleObj.permissionType === "REVIEW_CARD") {
          if (ruleObj.stateRule === cardStatus) {
            if (cardStatus === "COMPLETE") {
              this.isDisabled = true;
            }
          }
        }
      }
    });

    return this.isDisabled;

  }

  startNego() {
    this.loading = true;
    let negoObj: any = {};
    negoObj.negotiationId = this.negotiationId;
    negoObj.canvasId = this.canvasId;
    negoObj.userId = this.user.userId;
    // console.log(negoObj);
    this.negoService.startNegotiation(negoObj).then((success: UserNegotiation) => {
      // console.log(success);
      this.loading = false;
      // this.toast.showSnackBar("Successfully InterPlay Started");
      this.showStartNego = false;
      this.showDateToggle = false;
      // this.assignData(success);
      this.getStageDetailsByNegotiation();
    }).catch(err => {
      // this.toast.showSnackBar("Something went wrong");
    });


  }
  assignData(dataObj) {
    this.stageArr = this.orderPipe.transform(dataObj.stages, 'stageSequenceNumber', false);
    // console.log(this.stageArr);
    this.checkEditRule();
    let cardObj;
    let stageIndex;
    let cardIndex;
    // console.log(this.cardId);
    if (this.cardId != "") {
      this.stageArr.forEach((element, i) => {
        element.cards.find((item, j) => {
          if (item.cardId === this.cardId) {
            cardObj = item;
            stageIndex = i;
            cardIndex = j;
          }
        });
      });
      //console.log(cardObj);
      //console.log(this.user);
      //   if(this.user.role === 'PARTICIPANT'){
      //    let stageArr =  this.stageArr[this.stageIndex].cards.filter(item => item.cardStatus === 'COMPLETE');
      //    if(cardObj.cardStatus != 'COMPLETE')
      //    stageArr.cards.push(cardObj);
      //    this.stageArr = stageArr;
      //  }
      // else{
      //   let stageArr = this.stageArr.filter(stage => {
      //      stage.cards.filter(card => card.cardStatus != "NOT_STARTED")
      //   });
      //   this.stageArr = stageArr;
      //   console.log(this.stageArr);
      // }
      //console.log(cardObj);
      this.showCardForm(cardObj, stageIndex, cardIndex);
    }
    // this.loading = false;
    this.orchestrationName = dataObj.orchestrationCanvasName;
    this.teamObj = dataObj.users;
    this.showShedule = false;
    //console.log(date);
    this.negotiationName = dataObj.negotiationName;
    this.negotiationDesc = dataObj.negotiationDescription;
    if (this.fromInterPlay) {
      if (dataObj.negotiationStatus === 'CLOSED')
        this.templateName = "Closed InterPlay: " + this.negotiationName;

      else
        this.templateName = "Edit InterPlay: " + this.negotiationName;
    }
    else {
      if (dataObj.negotiationStatus === 'CLOSED')
        this.templateName = "InterPlay-as-actioned: " + this.negotiationName;

      else
        this.templateName = "InterPlay-in-action: " + this.negotiationName;
    }
  }

  showCardFormForFacilitator(cardObj, i, j) {
    // console.log(cardObj);
    this.cardStructureArr.push(cardObj);
    // console.log(this.cardStructureArr);
  }

  // edit card
  saveCardEdit() {
    this.loading = true;
    this.assignCardObj();
    //console.log(this.cardObj);
    this.userService.saveUserCard(this.cardObj).then(success => {
      // console.log(success);
      this.loading = false;
      // this.toast.showSnackBar("Successfully Card Saved");
    }).catch(err => {

    });
  }

  // done card
  doneCardEdit() {
    this.loading = true;
    this.assignCardObj();
    // console.log(this.cardObj);
    this.userService.doneUserCard(this.cardObj).then(success => {
      //console.log(success);
      this.disabledForm = true;
      this.showDoneBtn = false;
      // this.loading = false;
      // this.toast.showSnackBar("Successfully Card Done");
      setTimeout(() => {
        this.getRunningAndClosedNegotiationDetailsHandler();
      }, 2500);
    }).catch(err => {

    });
  }


  checkEditRule() {
    this.stageArr.forEach((item, i) => {
      item.cards.forEach((element, j) => {
        let editRule = false;
        if (element.cardStatus === "IN_PROGRESS") {
          let cardRuleObj: any = element.cardRights.find(item => {
            let ruleObj: any = item;
            if (ruleObj.stateRule === "IN_PROGRESS")
              return item;
          });
          if (cardRuleObj) {
            if (this.user.role === cardRuleObj.privacyRule) {
              if (cardRuleObj.permissionType === "EDIT_CARD")
                editRule = true;
            }
          }
        }

        this.stageArr[i].cards[j].editRule = editRule;
        // this.cardsArr[i].editRule = editRule;

      })
    })
    //console.log(this.stageArr);
  }

  //assign ard Obj for save and done
  assignCardObj() {
    //console.log(this.card);
    this.cardObj.cardId = this.card.cardId;
    this.cardObj.userId = this.user.userId;
    let cardStrucObj: CardStructure = new CardStructure();
    // cardStrucObj.userId = this.user.userId;
    // cardStrucObj.userFirstName = this.user.firstName;
    // cardStrucObj.userLastName = this.user.lastName;
    // cardStrucObj.userCardStatus = this.card.cardStatus;
    this.cardObj.cardStructure = [];
    //console.log(this.card.cardStructure);
    // this.cardObj.cardStructure = this.card.cardStructure;
    this.card.cardStructure.forEach((item: any) => {
      cardStrucObj = item;
      this.cardObj.cardStructure.push(cardStrucObj);
    })
    // cardStrucObj.cardStructure = this.card.cardStructure[0].cardStructure;
  }
}
