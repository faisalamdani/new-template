import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuideComponent } from './guide/guide.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { CalendarModule } from 'angular-calendar';
import { OrchestrationslistComponent } from './orchestrationslist/orchestrationslist.component';
import { ContactComponent } from './contact/contact.component';
import { AgmCoreModule } from '@agm/core';
import { CardViewComponent } from './card-view/card-view.component';
import { CardGridComponent } from './card-grid/card-grid.component';
import { InterplaylistComponent } from './interplaylist/interplaylist.component';
import { CardKanbanComponent } from './card-kanban/card-kanban.component';
import { OrchestrationskanbanComponent } from './orchestrationskanban/orchestrationskanban.component';
import { OrchestrationsCardComponent } from './orchestrations-card/orchestrations-card.component';
import { UsersComponent } from './users/users.component';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { OrderModule } from 'ngx-order-pipe';

import { TreeviewModule } from 'ngx-treeview';
import {MatMenuModule} from '@angular/material/menu';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

import {MatChipsModule} from '@angular/material/chips';
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list';
import { CardEditComponent } from './card-edit/card-edit.component';
import { OrchestrationsComponent } from './dialogs/orchestrations/orchestrations.component';
import { ConfirmationComponent } from './dialogs/confirmation/confirmation.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { StageComponent } from './dialogs/stage/stage.component';
import { PipesModule } from '../shared/pipes/pipes.module';



@NgModule({
  declarations: [
    GuideComponent,
    OrchestrationslistComponent,
    ContactComponent,
    CardViewComponent,
    CardGridComponent,
    InterplaylistComponent,
    CardKanbanComponent,
    OrchestrationskanbanComponent,
    OrchestrationsCardComponent,
    UsersComponent,
    CardEditComponent,
    OrchestrationsComponent,
    ConfirmationComponent,
    StageComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    PipesModule,
    NgxPaginationModule,
    MatMenuModule,
    MatChipsModule,
    MatTabsModule,
    MatListModule,
    MatSlideToggleModule,
    OrderModule,
    TreeviewModule.forRoot(),
    CalendarModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCb44fZMVNTqsA7phK5chbOolMgsJl9mFw'
    }),
  ],
  exports: [
    GuideComponent,
    OrchestrationslistComponent,
    ContactComponent,
    CardViewComponent,
    CardGridComponent,
    InterplaylistComponent,
    CardKanbanComponent,
    OrchestrationskanbanComponent,
    OrchestrationsCardComponent
  ],
  schemas: [NO_ERRORS_SCHEMA],
  entryComponents: [ 
    OrchestrationsComponent,
    ConfirmationComponent,
    StageComponent 
  ]
})
export class PagesModule { }
