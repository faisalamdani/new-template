import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrchestrationsCardComponent } from './orchestrations-card.component';

describe('OrchestrationsCardComponent', () => {
  let component: OrchestrationsCardComponent;
  let fixture: ComponentFixture<OrchestrationsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrchestrationsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrchestrationsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
