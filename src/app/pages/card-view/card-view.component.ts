import { Component, OnInit } from '@angular/core';
import { TreeviewItem, TreeviewConfig } from 'ngx-treeview';
import { User } from './../../models/user';
import {UserModel} from './../../models/user-model.model';
import {OrchestrationCanvasDto} from './../../models/orchestration-canvas';
import { Router, ActivatedRoute } from '@angular/router';
import {UserCardsService} from './../../services/user/user-cards.service';
import {OrchestratorService} from './../../services/orchestration-canvas-service/orchestrator.service';
@Component({
  selector: 'app-card-view',
  templateUrl: './card-view.component.html',
  styleUrls: ['./card-view.component.scss']
})
export class CardViewComponent implements OnInit {

  cardsArr: Array<UserModel> = new Array<UserModel>();
  cardsDisplayArr: Array<UserModel> = new Array<UserModel>();
  cardsFilteredArr: Array<UserModel> = new Array<UserModel>();
  orchestrationArr: Array<OrchestrationCanvasDto> = new Array<OrchestrationCanvasDto>();
  filterData = [];
  showFilter = true;
  setActive =  true;
  user: User;
  showCardGrid = true;
  loading = true;

  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: false,
    decoupleChildFromParent: false,
    maxHeight: 400,
  });
  items: TreeviewItem[] = [];
  negotiationId = "";
  constructor(
    private userCardService: UserCardsService,
    private orchService: OrchestratorService,
    private route: Router,
    private router: ActivatedRoute,
  ) {

  }

  ngOnInit() {


    this.user = JSON.parse(localStorage.getItem("userdata"));
    this.negotiationId = this.router.snapshot.paramMap.get('negotiationId');
    //console.log(this.router.snapshot.paramMap.get('cardView'));
    if (this.negotiationId != null)
    this.showCardGrid = JSON.parse(this.router.snapshot.paramMap.get('cardView'));
    //console.log(this.showCardGrid)
    this.userCardService.getUserCardsByUserId(this.user.userId, this.user.hostTenantId).subscribe((success) => {
     console.log(success);
      this.items = createDataForFilter(success[0]);
      this.items.forEach(item => {
        item.checked = false;
        item.collapsed = true;
        item["internalChildren"].forEach(item => { item.checked = false; })
      });

      let data: any = success;
      // if(this.user.role === 'PARTICIPANT')
      // {
      //   let userCards = data[0].userCards;
      //   userCards.filter(element => {
      //     if(element.cardStatus != "NOT_STARTED") {
      //       this.cardsArr.push(element);
      //     }
      //   });
      //   this.cardsDisplayArr = this.cardsArr;
      // }
      // else {
      // this.cardsArr = data[0].userCards;
      // this.cardsDisplayArr = data[0].userCards;
      // }
      // console.log(data[0].userCards);
      // console.log(this.negotiationId);
      if (this.negotiationId != null) {
        this.cardsArr = data[0].userCards.filter(item => item.negotiationId === this.negotiationId);
        this.cardsArr = this.cardsArr.sort((a: any, b: any) =>
        new Date(b.updatedDateTime).getTime() - new Date(a.updatedDateTime).getTime());
        this.cardsDisplayArr = this.cardsArr;
        this.showFilter = false;
      }
      else {
        this.cardsArr = data[0].userCards;
        this.cardsArr = this.cardsArr.sort((a: any, b: any) =>
        new Date(b.updatedDateTime).getTime() - new Date(a.updatedDateTime).getTime());
        this.cardsDisplayArr = this.cardsArr;
      }
      //console.log(this.cardsDisplayArr);
      this.cardsFilteredArr = this.cardsDisplayArr;
      this.checkEditRule();
      this.addFilter();
      // this.cardsDisplayArr = this.cardsArr.filter(element => {
      //   if(element.cardStatus === "IN_PROGRESS")
      //     return element; 
      // })
      // this.toggleActive();
      this.loading = false;
    })

    this.orchService.getOrchestrationsByTenantId(this.user.hostTenantId).subscribe(
      (data: Array<OrchestrationCanvasDto>) => {
        this.orchestrationArr = data;
        //console.log(this.orchestrationArr)
      },
      error => {
        //this.error = error
      }
    );
  }

  updateDisplayCardsArray()
  {

    //console.log(this.cardsDisplayArr);
    let cardsArr:Array<UserModel> = this.cardsDisplayArr;
    if(this.setActive)
    {
      this.cardsDisplayArr = cardsArr.filter(element => {
        if(element.cardStatus === "IN_PROGRESS" || element.cardStatus === "NOT_STARTED")
          return element; 
      })
     // console.log(this.cardsDisplayArr);
    }
    // else{
    //   this.cardsDisplayArr = this.cardsFilteredArr;
    // }
    //console.log(this.cardsDisplayArr);
  }
  onSelectedChange(event) {
    //console.log(event);
    let itemArr: any = [];
    this.items.forEach(element => {
      let e: TreeviewItem = element;
      e.children.filter(d => {
        if (event.find(x => x === d.value)) {
          let obj: any = {};
          obj.parent = e.text;
          obj.child = d.text;
          obj.value = d.value;
          itemArr.push(obj);
        }
      });

    });
   // console.log(itemArr);

    this.filterData = itemArr;
    this.addFilter();
  }

 

  checkEditRule() {
    this.cardsDisplayArr.forEach((element,i) => {
      let editRule = false;
      if(element.cardStatus === "IN_PROGRESS")
      {
    let cardRuleObj:any =  element.cardRights.find(item => {
        let ruleObj :any = item;
        if(ruleObj.stateRule === "IN_PROGRESS")
          return item;
      });
      if(cardRuleObj)
      {
        if(this.user.role === cardRuleObj.privacyRule)
          {
            if(cardRuleObj.permissionType === "EDIT_CARD")
              editRule = true;
          }
      }
    }
    // let privateKey = "Private";
    // let cardRuleObj:any =  element.cardRights.find(item => {
    //   let ruleObj :any = item;
    //   if(ruleObj.stateRule === "IN_PROGRESS")
    //     return item;
    // });
    // if(cardRuleObj)
    // {
    //   if(this.user.role === cardRuleObj.privacyRule)
    //     {
    //       if(cardRuleObj.permissionType === "EDIT_CARD")
    //       {
    //         if()
    //       }
          
    //     }
    // }
      this.cardsDisplayArr[i].editRule = editRule;
      this.cardsArr[i].editRule = editRule;
    })
  }

  toggleActive(){
    // this.addFilter();
    this.setActive = !this.setActive;
    this.addFilter();
    // this.updateDisplayCardsArray();
  }
  addFilter() {
    // this.toggleActive();
    this.cardsDisplayArr = this.cardsArr;

    var roleFilter = false;
    var statusFiter = false;
    var canvasFilter = false;
    var negFilter = false;
    var kanbanFilter = false;
    var stageNameFilter = false;
    var stateStandardNameFilter = false;
    var teamFilter = false;
   // console.log(this.filterData)
    this.filterData.forEach(x => {
      if (x.parent == "Roles")
        roleFilter = true;
      else
        if (x.parent == "Status")
          statusFiter = true;
        else
          if (x.parent == "Canvas")
            canvasFilter = true;
          else
            if (x.parent == "Kanban")
              kanbanFilter = true;
            else
              if (x.parent == "InterPlay")
                negFilter = true;
              else
                if (x.parent == "stageName")
                stageNameFilter = true;
                else
                  if (x.parent == "stateStandardName")
                  stateStandardNameFilter = true;
                    else
                    if (x.parent == "Teams")
                    teamFilter = true;
    });
    // this.cardsDisplayArr = this.cardsArr;
    if (this.filterData.length > 0) {
      if (statusFiter) {
        this.cardsDisplayArr = this.cardsDisplayArr.filter(element => {
          return this.filterData.find(x => {
            if (x.parent.toLowerCase() === "status" && x.child.toLowerCase() === element.cardStatus.toLowerCase())

              return element;
          })

        });
        
      }

      //console.log(this.cardsDisplayArr);
      if (roleFilter) {
        this.cardsDisplayArr = this.cardsDisplayArr.filter(element => {
          return this.filterData.find(x => {
            if (x.parent.toLowerCase() === "roles" && x.child.toLowerCase() === element.userRole.toLowerCase())

              return element;
          })

        });
      }



      if (kanbanFilter) {
        this.cardsDisplayArr = this.cardsDisplayArr.filter(element => {
          return this.filterData.find(x => {
            if (x.parent.toLowerCase() === "kanban" && x.child.toLowerCase() === element.kanbanType.toLowerCase())
              return element;
          })

        });
      }

      if (negFilter) {
        this.cardsDisplayArr = this.cardsDisplayArr.filter(element => {
          return this.filterData.find(x => {
            if (x.parent.toLowerCase() === "interplay" && x.child.toLowerCase() === element.negotiationName.toLowerCase())

              return element;
          })

        });
      }

      if (canvasFilter) {
        this.cardsDisplayArr = this.cardsDisplayArr.filter(element => {
          return this.filterData.find(x => {
            if (x.parent.toLowerCase() === "canvas" && x.child.toLowerCase() === element.orchestrationCanvasName.toLowerCase())

              return element;
          })

        });
      }

      if (teamFilter) {
        this.cardsDisplayArr = this.cardsDisplayArr.filter(element => {
          return this.filterData.find(x => {
            if (x.parent.toLowerCase() === "teams" && x.child.toLowerCase() === element.teamName.toLowerCase())

              return element;
          })

        });
      }
      if (stageNameFilter) {
        this.cardsDisplayArr = this.cardsDisplayArr.filter(element => {
          return this.filterData.find(x => {
            if (x.parent.toLowerCase() === "stageName" && x.child.toLowerCase() === element.orchestrationCanvasName.toLowerCase())

              return element;
          })

        });
      }

      if (stateStandardNameFilter) {
        this.cardsDisplayArr = this.cardsDisplayArr.filter(element => {
          return this.filterData.find(x => {
            if (x.parent.toLowerCase() === "stateStandardName" && x.child.toLowerCase() === element.orchestrationCanvasName.toLowerCase())

              return element;
          })

        });
      }





    }
    else {
      this.cardsDisplayArr = this.cardsArr;

    }
    this.updateDisplayCardsArray();    
  }

  goToCreateCard(item) {
    let obj: any = {};
    //console.log(item);
    obj.canvasId = item.canvasId;
    obj.orchestrationName = item.orchestrationName
    // this.route.navigate(["/facilitator-card/", quw);
    this.route.navigate(['/interplay/card-edit/'], { queryParams: obj });
  }

  removeFilerItem(event) {
    this.filterData = event.Arr;
    let value = event.item.value;

    this.items.forEach(element => {
      element.children.find(data => {
        //console.log(data);
        if (data.value === value)
          data.checked = false;
      })
    })
    //console.log(this.items);
    this.addFilter();
    // this.onSelectedChange(this.items);
  }

}
const createDataForFilter = function (data) {
  let returnArr: TreeviewItem[] = [];
  let val = 1;

  // let roles = data.roles;
  // let obj: any = {};
  // obj.children = [];
  // if (roles.length > 0) {
  //   obj.text = "Roles";
  //   obj.value = val++;
  //   obj.checked = false;
  //   roles.forEach(item => {
  //     let obj1: any = {};
  //     obj1.text = item;

  //     obj1.value = val++;
  //     obj1.checked = false;
  //     obj.children.push(obj1);
  //   });

  //   let r = new TreeviewItem(obj);
  //   returnArr.push(r);
  // }

  // let status = data.status;
  // let objStatus: any = {};
  // if (status.length > 0) {
  //   objStatus.text = "Status";
  //   objStatus.value = val++;
  //   objStatus.checked = false;
  //   objStatus.children = [];
  //   status.forEach(item => {
  //     let obj1: any = {};
  //     // janarasConstant.forEach(element => {
  //     //   if(element.key === item)
  //     //     obj1.text = element.value;
  //     // })
  //     obj1.text = item;
  //     obj1.value = val++;
  //     obj1.checked = false;
  //     objStatus.children.push(obj1);
  //   });

  //   let r = new TreeviewItem(objStatus);
  //   returnArr.push(r);
  // }

  let teams = data.teams;
  let objTeams: any = {};
  if (teams.length > 0) {
    objTeams.text = "Teams";
    objTeams.value = val++;
    objTeams.checked = false;
    objTeams.children = [];
    teams.forEach(item => {
      let obj1: any = {};
      obj1.text = item;
      obj1.value = val++;
      obj1.checked = false;
      objTeams.children.push(obj1);
      
    });
    
    let r = new TreeviewItem(objTeams);
    returnArr.push(r);
  }

  let nego = data.negotiationNames;
  let objNego: any = {};
  if (nego.length > 0) {
    objNego.text = "InterPlay";
    objNego.value = val++;
    objNego.checked = false;
    objNego.children = [];
    nego.forEach(item => {
      let obj1: any = {};
      obj1.text = item;
      obj1.value = val++;
      obj1.checked = false;
      objNego.children.push(obj1);
    });

    let r = new TreeviewItem(objNego);
    returnArr.push(r);
  }

  // let kanban = data.kanbanTypes;
  // let objKanban: any = {};
  // if (kanban.length > 0) {
  //   objKanban.text = "Kanban";
  //   objKanban.value = val++;
  //   objKanban.checked = false;
  //   objKanban.children = [];
  //   kanban.forEach(item => {
  //     let obj1: any = {};
  //     obj1.text = item;
  //     obj1.value = val++;
  //     obj1.checked = false;
  //     objKanban.children.push(obj1);
  //   });

  //   let r = new TreeviewItem(objKanban);
  //   returnArr.push(r);
  // }

  let orchCanvas = data.orchestrationCanvas;
  let objOrchCanvas: any = {};
  if (orchCanvas.length > 0) {
    objOrchCanvas.text = "Canvas";
    objOrchCanvas.value = val++;
    objOrchCanvas.checked = false;
    objOrchCanvas.children = [];
    orchCanvas.forEach(item => {
      let obj1: any = {};
      obj1.text = item;
      obj1.value = val++;
      obj1.checked = false;
      objOrchCanvas.children.push(obj1);
      
    });
    
    let r = new TreeviewItem(objOrchCanvas);
    returnArr.push(r);
  }

  

   

  //   new TreeviewItem({
  //   text: 'Children', value: 1, collapsed: true, children: [
  //       { text: 'Baby 3-5', value: 11 },
  //       { text: 'Baby 6-8', value: 12 },
  //       { text: 'Baby 9-12', value: 13 }
  //   ]
  // })
  return returnArr;
};