import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { OrchestratorService } from 'src/app/services/orchestration-canvas-service/orchestrator.service';
import { OrchestrationCanvasDto } from 'src/app/models/orchestration-canvas';
import { MdbTablePaginationComponent, MdbTableDirective, ToastService } from 'ng-uikit-pro-standard';
import { MDBModalRef, MDBModalService } from 'ng-uikit-pro-standard';
import { OrchestrationsComponent } from '../dialogs/orchestrations/orchestrations.component';
import { ConfirmationComponent } from '../dialogs/confirmation/confirmation.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orchestrationslist',
  templateUrl: './orchestrationslist.component.html',
  styleUrls: ['./orchestrationslist.component.scss']
})
export class OrchestrationslistComponent implements OnInit, AfterViewInit {

  userdata: any;
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective
  modalRef: MDBModalRef;

  canvasList: object[] = [];
  private sorted = false;
  previous: any = [];
  loading: boolean = true;
  options = { positionClass: 'md-toast-bottom-right' };
  addOcData: OrchestrationCanvasDto;

  constructor(
    private canvasService: OrchestratorService,
    private cdRef: ChangeDetectorRef,
    private modalService: MDBModalService,
    private toast: ToastService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.userdata = JSON.parse(localStorage.getItem("userdata"));
    if (this.userdata.role == "ORCHESTRATOR") {
      // this.addOrchBtnPer = true;
      //this.access = false;
      this.getOrchestrations(this.userdata.hostTenantId, this.userdata.userId);
    } else {
      //this.access = true;
    }

  }

  getOrchestrations(hostTenantId, userId) {

    this.canvasService.getAllOrchestrationsByOrchestratorId(hostTenantId, userId).subscribe(
      (data: Array<OrchestrationCanvasDto>) => {
        // this.canvasList = this.orderPipe.transform(data, 'createdDateTime', true);
        // this.wait = false;
        //this.canvasList = data;
        this.mdbTable.setDataSource(data);
        this.canvasList = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
        this.loading = false;
      },
      error => {
        this.loading = false;
        // this.wait = false;
        //this.error = error
      }
    );

  }

  addOrchestrations(canvas: OrchestrationCanvasDto) {
    this.modalRef = this.modalService.show(OrchestrationsComponent, {
      class: 'modal-dialog modal-dialog-centered',
      containerClass: 'right',
      animated: true,
      data: {
        canvasData: canvas
    }
    });

    this.modalRef.content.action.subscribe( (result: any) => { 
      if (result == 'add') {
        this.toast.success('Successfully add orchatratation', '', this.options);
        this.ngOnInit();
      } else if (result == "error") {
        this.toast.error("Something went wrong", '', this.options);
      } else if (result == "edit") {
        this.toast.success('Successfully update orchatratation', '', this.options);
        this.ngOnInit();
      }
    });

  }

  deleteOrchestration(canvas){
    
    this.modalRef = this.modalService.show(ConfirmationComponent, {
      class: 'modal-dialog modal-sm modal-notify modal-danger',
      animated: true,
    });

    this.modalRef.content.action.subscribe( (result: any) => { 
      if (result == 'Yes') {
        this.toast.info('Processing...', '', this.options);
        this.canvasService.deleteOrchestrationCanvasById(canvas).subscribe(
          data => {
            
            this.toast.success('Successfully delete orchatratation', '', this.options);
            this.ngOnInit();
          },
          error => {
            this.toast.error("Something went wrong", '', this.options);
          }
        );
        
      } else if (result == "No") {

      } else{
        this.toast.error("Something went wrong", '', this.options);
      }
    });

  }

  addStageAndCard(canvas) {
    localStorage.setItem('canvas', JSON.stringify(canvas));
    this.router.navigate(["orchestrations/orchestrationskanban/" + canvas.canvasId]);
  }

  sortBy(by: string | any): void {

    this.canvasList.sort((a: any, b: any) => {
      if (a[by] < b[by]) {
        return this.sorted ? 1 : -1;
      }
      if (a[by] > b[by]) {
        return this.sorted ? -1 : 1;
      }

      return 0;
    });

    this.sorted = !this.sorted;
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(10);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

}
