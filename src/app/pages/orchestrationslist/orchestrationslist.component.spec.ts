import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrchestrationslistComponent } from './orchestrationslist.component';

describe('OrchestrationslistComponent', () => {
  let component: OrchestrationslistComponent;
  let fixture: ComponentFixture<OrchestrationslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrchestrationslistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrchestrationslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
