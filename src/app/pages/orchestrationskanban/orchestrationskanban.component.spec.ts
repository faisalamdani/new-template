import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrchestrationskanbanComponent } from './orchestrationskanban.component';

describe('OrchestrationskanbanComponent', () => {
  let component: OrchestrationskanbanComponent;
  let fixture: ComponentFixture<OrchestrationskanbanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrchestrationskanbanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrchestrationskanbanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
