import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { OrchestratorService } from 'src/app/services/orchestration-canvas-service/orchestrator.service';
import { ToastService, MDBModalService, MDBModalRef } from 'ng-uikit-pro-standard';
import { CanvasDetailsDTO } from 'src/app/models/canvasDetails';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Stages } from 'src/app/models/stages';
import { StageComponent } from '../dialogs/stage/stage.component';

@Component({
  selector: 'app-orchestrationskanban',
  templateUrl: './orchestrationskanban.component.html',
  styleUrls: ['./orchestrationskanban.component.scss']
})
export class OrchestrationskanbanComponent implements OnInit {

  canvasId: string;
  canvas: any;
  stageDetails: any;
  wait: boolean = true;
  numberOfStage: number;
  user: User;
  addStageData: Stages;
  modalRef: MDBModalRef;
  options = { positionClass: 'md-toast-bottom-right' };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private canvasService: OrchestratorService,
    private toast: ToastService,
    private modalService: MDBModalService,
  ) { }

  ngOnInit(): void {

    this.user = JSON.parse(localStorage.getItem("userdata"));
    this.canvasId = this.route.snapshot.paramMap.get('canvasId');

    if (this.canvasId != JSON.parse(localStorage.getItem("canvas")).canvasId) {
      this.router.navigate(["/orchestrations"]);
    } else {
      this.canvas = JSON.parse(localStorage.getItem("canvas"));
      this.getStageDetails(this.canvasId);
    }

  }

  getStageDetails(canvasId: string) {

    this.canvasService.getAllStagesInOrchestration(canvasId, this.user.hostTenantId).subscribe(
      (data: CanvasDetailsDTO) => {
        this.stageDetails = data.stages;
        this.numberOfStage = this.stageDetails.length;
        this.wait = false;
      },
      error => {
        this.wait = false;
      }
    );

  }

  addStage(stage: Stages, nextStage: string) {

    if (nextStage == "next_stage") {

      this.modalRef = this.modalService.show(StageComponent, {
        class: 'modal-dialog modal-dialog-centered',
        data: {
          title: 'Add Next Stage',
          canvasData: localStorage.getItem("canvas"),
          stageData: '',
          numberOfStages: stage.stageSequenceNumber + 1
        }
      });

    } else if (stage) {

      this.modalRef = this.modalService.show(StageComponent, {
        class: 'modal-dialog modal-dialog-centered',
        data: {
          title: 'Edit Stage',
          canvasData: localStorage.getItem("canvas"),
          stageData: stage,
          numberOfStages: this.numberOfStage + 1
        }
      });

    } else {

      this.modalRef = this.modalService.show(StageComponent, {
        class: 'modal-dialog modal-dialog-centered',
        data: {
          title: 'Add Stage',
          canvasData: localStorage.getItem("canvas"),
          stageData: '',
          numberOfStages: this.numberOfStage + 1
        }
      });

    }

    this.modalRef.content.action.subscribe( (result: any) => { 
      if (result == 'add') {
        this.toast.success('Successfully add stage', '', this.options);
        this.ngOnInit();
      } else if (result == "error") {
        this.toast.error("Something went wrong", '', this.options);
      } else if (result == "edit") {
        this.toast.success('Successfully update stage', '', this.options);
        this.ngOnInit();
      }
    });

  }

  drop(event: CdkDragDrop<string[]>) {

    // let data = {
    //   cardId: event.item.data.cardId,
    //   canvasId: this.canvasId,
    //   stageId: event.container.id
    // };

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);

      this.moveItemInArrayStage(event.container.data, event.container.id)

    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

      this.moveItemInArrayStage(event.container.data, event.container.id);
      this.moveItemInArrayStage(event.previousContainer.data, event.previousContainer.id);
    }

    //this.canvasService.canvasKanbanMoveCard(data).subscribe();
  }

  moveItemInArrayStage(data, stageId) {
    let moveData = [];
    for (let i = 0; i <= data.length - 1; i++) {
      moveData.push({
        cardId: data[i].cardId,
        canvasId: this.canvasId,
        stageId: stageId,
        cardSequenceNumber: i + 1
      })
    }

    this.canvasService.canvasKanbanMoveCard(moveData).subscribe();
    this.getStageDetails(this.canvasId);
  }

  addCard(stage: Stages, stageDetails: Array<Stages>) {

  }

  editCard(stage: Stages, card: any, stageDetails: Array<Stages>) {

  }

  deleteStage(stage) {

  }

  deleteCard(stage, card) {

  }

}
