import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  @ViewChild('sidenav', {static: true}) sidenav: ElementRef;

  clicked: boolean;
  userData:any;
  userName:string = "";

  constructor(
    private router: Router,
  ) {
    this.clicked = this.clicked === undefined ? false : true;
  }

  ngOnInit() {
    //console.log(JSON.stringify(localStorage.getItem('loginUserData')).idToken);
    this.userData = JSON.parse(localStorage.getItem('userdata'));
    if(this.userData){
      this.userName = this.userData.firstName + " " + this.userData.lastName;
    }
  }

  setClicked(val: boolean): void {
    this.clicked = val;
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['/']);
  }

}
