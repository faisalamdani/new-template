export const environment = {
  production: false,
  confirm: {
    email: '',
    password: ''
  },
  baseHref: '/',
  javaApiUrl: "https://19vna2mm0k.execute-api.us-east-1.amazonaws.com/Prod",
  phpApiUrl: "https://bwqt1fpig0.execute-api.us-east-2.amazonaws.com/production",
  environmentName:"Development"
};
