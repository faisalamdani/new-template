export const environment = {
  production: true,
  confirm: {
    email: '',
    password: ''
  },
  baseHref: '/',
  javaApiUrl: "https://a2aedruts6.execute-api.us-east-1.amazonaws.com/Prod",
  phpApiUrl: "https://ezdvxrtzuc.execute-api.us-east-1.amazonaws.com/production",
  environmentName:"Production"
};
