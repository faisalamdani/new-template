// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`. 
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  confirm: {
    email: '',
    password: ''
  },
  baseHref: '/',
  //community url
  javaApiUrl: "https://a2aedruts6.execute-api.us-east-1.amazonaws.com/Prod",

   //test url
  // javaApiUrl: "https://19vna2mm0k.execute-api.us-east-1.amazonaws.com/Prod",
  
  phpApiUrl: "https://ezdvxrtzuc.execute-api.us-east-1.amazonaws.com/production",
  environmentName:"Test"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
